package com.picom.unionui.vroundwidgetlibrary;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import com.picom.sixtysecond.R;


/**
 * Класс, отображающий картинку кружочком
 * Created by Kos on 13.02.2015.
 */
public class VRoundImageView extends android.support.v7.widget.AppCompatImageView {


	int borderColor = Color.WHITE;
	float borderWidth = 1;
	float borderPadding = 1;

	public static final int PAINT_FLAGS = Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG | Paint.FILTER_BITMAP_FLAG;

	public VRoundImageView(Context context) {
		super(context);
	}

	public VRoundImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		readAttr(context, attrs);
	}

	private void readAttr(Context context, AttributeSet attrs) {
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.VRoundImageView);
		borderColor = a.getColor(R.styleable.VRoundImageView_borderColor, Color.WHITE);
		borderWidth = a.getDimension(R.styleable.VRoundImageView_borderRoundWidth, 1);
		borderPadding = a.getDimension(R.styleable.VRoundImageView_borderPadding, 1);
		a.recycle();
	}

	public VRoundImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		readAttr(context, attrs);
	}

	@Override
	protected void onDraw(Canvas canvas) {

		Drawable drawable = getDrawable();

		if (drawable == null) {
			return;
		}

		if (getWidth() == 0 || getHeight() == 0) {
			return;
		}
/*
		Bitmap b;
		if (drawable instanceof GlideBitmapDrawable)
		{
			b=((GlideBitmapDrawable) drawable).getBitmap();
		}
		else
		if (drawable instanceof BitmapDrawable)
		{
			b = ((BitmapDrawable) drawable).getBitmap();
		}
		else

		{
			if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
				b = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
			} else {
				b = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
			}
			Canvas canvas2 = new Canvas(b);
			drawable.setBounds(0, 0, canvas2.getWidth(), canvas2.getHeight());
			drawable.draw(canvas2);
		}




		Bitmap bitmap = b.copy(b.getConfig(), true);
*/
		float padding = getPaddingLeft() + borderPadding + borderWidth;
		float w = getWidth() - (padding) * 2;//, h = getHeight();
		//		Log.d("Kos","width "+w );
		// getCroppedBitmap(b, w/2,canvas);


		Bitmap roundBitmap = getCroppedBitmap(drawable, w, canvas, padding);


		//	canvas.drawARGB(0, 0, 0, 0);
		canvas.drawColor(Color.TRANSPARENT);
		canvas.drawBitmap(roundBitmap, padding, padding, null);
		if (borderWidth > 0)
			drawCircle(canvas, padding, w * 0.5f, borderWidth, borderPadding, borderColor);


	}

	private static void drawCircle(Canvas canvas, float padding, float radius, float borderWidth, float borderPadding, int borderColor) {

		Paint pen = new Paint(PAINT_FLAGS);
		pen.setStyle(Paint.Style.STROKE);
		pen.setStrokeWidth(borderWidth);
		pen.setColor(borderColor);
		canvas.drawCircle(padding + radius, padding + radius, borderPadding + radius, pen);

	}

	public static Bitmap getCroppedBitmap(Bitmap sbmp, float width, Canvas canvass) {
		 /*   Bitmap sbmp;
		   // if(bmp.getWidth() != radius || bmp.getHeight() != radius)
                sbmp = Bitmap.createScaledBitmap(bmp, radius*2, radius*2, false);
          //  else
         //       sbmp = bmp;*/
		float radius = width * 0.5f;
		Bitmap output = Bitmap.createBitmap((int) width,
				(int) width, Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(output);


		final Paint paint = new Paint(PAINT_FLAGS);
		final Rect rect = new Rect(0, 0, sbmp.getWidth(), sbmp.getHeight());
		final Rect dstrect = new Rect(0, 0, (int) width, (int) width);


		canvas.drawColor(Color.TRANSPARENT);


		paint.setColor(Color.GRAY);
		canvas.drawCircle(radius, radius, radius, paint);
		paint.setXfermode(xfermote);
		canvas.drawBitmap(sbmp, rect, dstrect, paint);

		//  return null;
		return output;
	}

	public static Bitmap getCroppedBitmap(Drawable drawable, float width, Canvas canvass, float padding) {
		Bitmap sbmp;


		float radius = width * 0.5f;
		Bitmap output = Bitmap.createBitmap((int) width, (int) width, Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final Rect dstrect = new Rect(0, 0, (int) width, (int) width);

		sbmp = Bitmap.createBitmap(dstrect.width(), dstrect.height(), Bitmap.Config.ARGB_8888);


		Canvas canvas2 = new Canvas(sbmp);
		//*
		canvas2.translate(
				(dstrect.width() - drawable.getIntrinsicWidth()) / 2,
				(dstrect.height() - drawable.getIntrinsicHeight()) / 2);
		//drawable.setBounds(0, 0, canvas2.getWidth(), canvas2.getHeight());
/*/
		final float k=width/
				(float) Math.sqrt(
				( (drawable.getIntrinsicWidth()* drawable.getIntrinsicWidth())+
						drawable.getIntrinsicHeight()* drawable.getIntrinsicHeight()));
		canvas2.translate(
				(dstrect.width() - drawable.getIntrinsicWidth()*k)/(2),
				(dstrect.height()- drawable.getIntrinsicHeight()*k)/(2));
//		final float k=width/(width+padding);
		canvas2.scale(k,k);

	//*/
		drawable.draw(canvas2);


		final Rect rect = new Rect(0, 0, canvas2.getWidth(), canvas2.getHeight());

		canvas.drawColor(Color.TRANSPARENT);

		canvas.drawCircle(radius, radius, radius, circlePen);
		canvas.drawBitmap(sbmp, rect, dstrect, porterPaint);

		//  return null;
		return output;
	}

	static PorterDuffXfermode xfermote = new PorterDuffXfermode(PorterDuff.Mode.SRC_IN);
	static Paint circlePen;
	static Paint porterPaint;

	static {
		circlePen = new Paint(PAINT_FLAGS);
		circlePen.setColor(Color.GRAY);
		porterPaint = new Paint(circlePen);
		porterPaint.setXfermode(xfermote);
	}

	public int getBorderColor() {
		return borderColor;
	}

	/**
	 * @param borderColor
	 * @attr ref android.R.styleable#VRoundImageView_borderColor
	 */
	public void setBorderColor(int borderColor) {
		this.borderColor = borderColor;
		this.requestLayout();
	}

	public float getBorderWidth() {
		return borderWidth;
	}

	/**
	 * @param borderWidth
	 * @attr ref android.R.styleable#VRoundImageView_borderWidth
	 */
	public void setBorderWidth(float borderWidth) {
		this.borderWidth = borderWidth;
		this.requestLayout();
	}

	public float getBorderPadding() {
		return borderPadding;
	}

	/**
	 * @param borderPadding
	 * @attr ref android.R.styleable#VRoundImageView_borderPadding
	 */
	public void setBorderPadding(float borderPadding) {
		this.borderPadding = borderPadding;
		this.requestLayout();
	}
}
