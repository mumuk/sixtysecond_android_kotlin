package com.picom.unionui.helpers

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore

object IntentHelper {

	const val ACTION_IDS = "open_action_ids"

	fun callPhone(context: Context, phone: String) {
		var phone = phone
		try {
			phone = phone.replace("-", "").replace(" ", "").replace("(", "").replace(")", "")
			val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:$phone"))
			context.startActivity(intent)
		} catch (ignored: Exception) {
		}

	}

	fun sendEmail(context: Context, email: String) {
		try {
			val intent = Intent(Intent.ACTION_SEND)
			intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
			intent.type = "message/rfc822"
			context.startActivity(intent)
		} catch (ignored: Exception) {
		}

	}

	fun startBrowser(context: Context, uri: String?) {
		if (uri != null) {
			startBrowser(context, Uri.parse(uri))
		}
	}

	fun startBrowser(context: Context, uri: Uri?) {
		try {
			if (uri != null) {
				val ignored = Intent(Intent.ACTION_VIEW, uri)
				context.startActivity(ignored)
			}
		} catch (ignored: Exception) {

		}

	}

	fun isIntentAvailable(ctx: Context, intent: Intent): Boolean {
		val packageManager = ctx.packageManager
		val list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)
		return list.size > 0
	}


	fun show(context: Context, clazz: Class<*>, ids: IntArray) {
		val intent = Intent(context, clazz)
		intent.putExtra(ACTION_IDS, ids)
		context.startActivity(intent)
	}


}
