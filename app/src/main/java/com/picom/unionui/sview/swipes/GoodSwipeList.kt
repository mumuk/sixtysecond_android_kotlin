package com.picom.unionui.sview.swipes

import android.support.v4.widget.SwipeRefreshLayout
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.View
import com.picom.sixtysecond.R
import com.picom.sixtysecond.adapters.IConferenceAdapter
import com.picom.unionui.sview.SActivity

/**
 * Created by picom on 24.09.2015.
 */
class GoodSwipeList : OnRefreshListener {

	var swipeLayout: SwipeRefreshLayout? = null
	var list: RecyclerView? = null

	private var conferenceAdapter: IConferenceAdapter<out RecyclerView.ViewHolder>? = null

	/**
	 * Эта функция создаёт все элементы
	 *
	 * @param activity
	 * @param adapter
	 */
	fun <T : RecyclerView.ViewHolder> initialize(activity: SActivity,
												 adapter: IConferenceAdapter<T>,
												 swipeCallback: ItemTouchHelper.Callback): RecyclerView? {
		list = activity.find(R.id.list)
		swipeLayout = activity.find(R.id.refresh)
		return initList(adapter, swipeCallback)

	}

	/**
	 * Эта функция создаёт все элементы
	 *
	 * @param topView
	 * @param adapter
	 */
	fun <T : RecyclerView.ViewHolder> initializeView(topView: View,
													 adapter: IConferenceAdapter<T>,
													 swipeCallback: ItemTouchHelper.Callback?): RecyclerView? {

		list = topView.findViewById<RecyclerView>(R.id.list)
		swipeLayout = topView.findViewById<SwipeRefreshLayout>(R.id.refresh)
		return initList(adapter, swipeCallback)
	}


	private fun <T : RecyclerView.ViewHolder> initList(adapter: IConferenceAdapter<T>,
													   swipeCallback: ItemTouchHelper.Callback?): RecyclerView? {
		conferenceAdapter = adapter
		swipeLayout?.setOnRefreshListener(this)

		list?.let { list ->
			list.adapter = conferenceAdapter
			val layoutManager = LinearLayoutManager(list.context, LinearLayoutManager.VERTICAL, false)
			list.addItemDecoration(DividerItemDecoration(list.context, LinearLayoutManager.VERTICAL))

			list.layoutManager = layoutManager


			//=== Добавление действия смахивания
			if (swipeCallback != null) {
				val itemTouchHelper = ItemTouchHelper(swipeCallback)
				itemTouchHelper.attachToRecyclerView(list)
			}
		}

		return list
	}


	override fun onRefresh() {
		conferenceAdapter?.let {
			it.reload()
			swipeLayout?.isRefreshing = true
		}
	}


	/**
	 * Вызывайте эту функцию если надо обновить весь список
	 */
	fun update() {
		conferenceAdapter?.changeArray()
		swipeLayout?.isRefreshing = false
	}

	fun startLoad() {
		swipeLayout?.isRefreshing = true
	}

	fun cancelLoad() {
		swipeLayout?.isRefreshing = false
	}

	fun showRefresh(isShow: Boolean) {
		if (isShow) {
			startLoad()
		} else {
			cancelLoad()
		}
	}

}
