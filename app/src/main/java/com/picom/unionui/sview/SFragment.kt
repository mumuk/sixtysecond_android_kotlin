package com.picom.unionui.sview

import android.content.Intent
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import com.picom.sixtysecond.R

open class SFragment : Fragment() {

	fun <T : View> find(rootView: View, resourceId: Int): T =
			rootView.findViewById<T>(resourceId)


	fun fragmentManager() = childFragmentManager


	fun <T> show(clazz: Class<T>) {
		val intent = Intent(this.activity, clazz)
		startActivity(intent)
	}

	fun <T> show(clazz: Class<T>, id: Int) {
		if (id > 0) {
			val intent = Intent(this.activity, clazz)
			intent.putExtra(SActivity.ACTION_ID, id)
			startActivity(intent)
		}
	}

	fun <T> show(clazz: Class<T>, idTitle: Int, idSeason: Int, ids: IntArray) {
		val intent = Intent(this.activity, clazz)
		intent.putExtra(SActivity.ARG_TABLE_NUMBER, idTitle)
		intent.putExtra(SActivity.ARG_TABLE_SEASON, idSeason)
		intent.putExtra(SActivity.ARG_TABLE_IDS, ids)
		startActivity(intent)

	}

	protected fun addHomeBtn(rootView: View, @IdRes toolBarId: Int, title: String, isNavigation: Boolean): Toolbar? {
		val toolbar: Toolbar? = rootView.findViewById(toolBarId)
		if (toolbar != null) {

			val act = activity
			when (act) {
				is AppCompatActivity -> {
					act.setSupportActionBar(toolbar)
					val actionBar: ActionBar? = act.supportActionBar
					if (actionBar != null) {
						actionBar.setDisplayHomeAsUpEnabled(true)
						actionBar.title = title

					}
					if (isNavigation) {
						toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp)
					} else {
						toolbar.setNavigationIcon(R.drawable.ic_back_arrow_24dp)
					}
					toolbar.title = title
				}
			}
		}
		return toolbar
	}

	protected fun addHomeBtn(rootView: View, @IdRes toolBarId: Int, title: String, subTitle: String): Toolbar? {
		val toolbar: Toolbar? = rootView.findViewById(toolBarId)
		if (toolbar != null) {

			val act = activity
			when (act) {
				is AppCompatActivity -> {
					act.setSupportActionBar(toolbar)
					val actionBar: ActionBar? = act.supportActionBar
					if (actionBar != null) {
						actionBar.setDisplayHomeAsUpEnabled(true)
						actionBar.title = title
						actionBar.subtitle = subTitle

					}
					toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp)
					toolbar.title = title
				}
			}
		}
		return toolbar
	}


}