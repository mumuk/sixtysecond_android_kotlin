package com.picom.unionui.sview

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast

@SuppressLint("Registered")
open class SActivity : AppCompatActivity(), View.OnClickListener {

	override fun onClick(view: View) {

	}

	fun <T : View> find(resourceId: Int): T? = findViewById<T?>(resourceId)

	fun click(resourceId: Int, body: () -> Unit) {
		findViewById<View>(resourceId).setOnClickListener { v: View -> body() }
	}

	fun fragmentManager(): FragmentManager = supportFragmentManager

	fun <T> show(clazz: Class<T>) {
		val intent = Intent(this, clazz)
		startActivity(intent)
	}

	fun <T> show(clazz: Class<T>, id: Int) {
		if (id > 0) {
			val intent = Intent(this, clazz)
			intent.putExtra(SActivity.ACTION_ID, id)
			startActivity(intent)
		}
	}

	fun <T> show(clazz: Class<T>, idTitle: Int, idSeason: Int, ids: Array<Int>) {
		val intent = Intent(this, clazz)

		intent.putExtra(SActivity.ARG_TABLE_NUMBER, idTitle)
		intent.putExtra(SActivity.ARG_TABLE_SEASON, idSeason)
		intent.putExtra(SActivity.ARG_TABLE_IDS, ids.joinToString(" "))
		startActivity(intent)

	}


	fun inflate(layoutResId: Int, rootView: ViewGroup) = {
		layoutInflater.inflate(layoutResId, rootView, false)
	}

	fun toast(message: String): Unit = Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

	fun toast(messageResId: Int): Unit = Toast.makeText(this, messageResId, Toast.LENGTH_SHORT).show()

	fun hideKeyboard() {
		val view = this.currentFocus
		if (view != null) {
			val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
			imm.hideSoftInputFromWindow(view.windowToken, 0)
		}
	}

//		fun alertYesNo(resTitle: Int, resInfo: Int, yesOperator: () -> Unit, noOperator: () -> Unit): Unit {
//
//			val alertDialog: AlertDialog = AlertDialog.Builder(this).create()
//			alertDialog.setTitle(resTitle)
//			alertDialog.setMessage(getString(resInfo))
//			alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(android.R.string.yes),
//					DialogInterface.OnClickListener {
//				fun onClick(dialog: DialogInterface, which: Int) ->
//				yesOperator()
//			}
//			})
//			alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(android.R.string.no),
//					DialogInterface.OnClickListener() {
//				fun onClick(dialog: DialogInterface, which: Int): Unit {
//				noOperator()
//			}
//			})
//
//			alertDialog.show()
//
//
//
//		}


	companion object {
		const val ACTION_ID = "open_action_id"
		const val ACTION_SUB_ID = "open_action_sub_id"
		const val ACTION_IDS = "open_action_ids"
		const val ARGUMENT_PAGE_NUMBER = "arg_page_number"
		const val ARG_TABLE_NUMBER: String = "table_number"
		const val ARG_TABLE_SEASON: String = "table_season"
		const val ARG_TABLE_IDS: String = "table_ids"
		const val KEY_SAVED_FILTER_CONSTRAINT = "table_search_value"
		const val KEY_SAVED_SELECTED_CONSTRAINT = "table_selected_value"
	}

}