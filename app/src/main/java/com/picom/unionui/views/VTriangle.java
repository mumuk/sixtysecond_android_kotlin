package com.picom.unionui.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;

/**
 * Created by picom on 12.04.2016.
 */
public class VTriangle extends android.support.v7.widget.AppCompatTextView {

	private Paint pen = new Paint();
	private float mAngle = 0;
	private Path path = new Path();
	private static final float cos30 = (float) (Math.sqrt(3) / 4);
	private static final float sin30 = (float) (0.5f);

	public VTriangle(Context context) {
		super(context);
		init(context);
	}

	public VTriangle(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public VTriangle(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context);
	}


	private void init(Context context) {
		pen.setStyle(Paint.Style.FILL);
	}

	public void setColor(int color) {
		if (pen.getColor() != color) {
			pen.setColor(color);
			invalidate();
		}
	}

	public void setRotate(float angle) {
		this.mAngle = angle;
		invalidate();
	}

	public void setColorAndRotate(int color, float angle) {
		pen.setColor(color);
		this.mAngle = angle;
		invalidate();
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {

		float x = (w - getPaddingLeft() - getPaddingRight());
		path.reset();
//		path.moveTo(-sin30*x,-cos30*x);
//		path.lineTo(+sin30*x,-cos30*x);
//		path.lineTo(0,cos30*x);

		float ySdvig = x / 8;
		path.moveTo(-sin30 * x, -cos30 * x + ySdvig);
		path.lineTo(+sin30 * x, -cos30 * x + ySdvig);
		path.lineTo(0, cos30 * x + ySdvig);
		path.close();

	}

	@Override
	protected void onDraw(Canvas canvas) {

		int w = canvas.getWidth();
		int h = canvas.getHeight();


		canvas.save();
		canvas.translate(w / 2, h / 2);
		canvas.rotate(mAngle);
		canvas.drawPath(path, pen);

		canvas.restore();

		//	canvas.drawText(getText(),);

		super.onDraw(canvas);
	}
}
