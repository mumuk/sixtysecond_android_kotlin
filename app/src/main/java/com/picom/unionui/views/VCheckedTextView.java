package com.picom.unionui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by picom on 06.10.2015.
 */
public class VCheckedTextView extends android.support.v7.widget.AppCompatCheckedTextView implements View.OnClickListener {


	public VCheckedTextView(Context context) {
		super(context);
		init();
	}

	public VCheckedTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public VCheckedTextView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init();
	}


	private void init() {
		this.setOnClickListener(this);
	}


	@Override
	public void onClick(View v) {
		this.toggle();
	}
}
