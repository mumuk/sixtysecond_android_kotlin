package com.picom.sixtysecond.inet

import android.view.View
import com.picom.unionui.sview.swipes.GoodSwipeList

class NoInternet(
		private val swipe: GoodSwipeList,
		private val noInternet: View,
		private val dataView: View,
		private val isLoading: () -> Boolean,
		private val hasData: () -> Boolean,
		private val startLoad: () -> Unit
) {

	fun gone(view: View, visible: Boolean) {
		view.visibility = if (visible) View.VISIBLE else View.GONE
	}

	fun gone(view: View) {
		view.visibility = View.GONE
	}

	fun show(view: View) {
		view.visibility = View.VISIBLE
	}

	fun showData() {
		//	gone(progressView)
		gone(noInternet)
		show(dataView)
		swipe.showRefresh(false)
	}

	fun showProgress() {
		//	show(progressView)
		gone(noInternet)
		//	gone(dataView)
		swipe.showRefresh(true)
	}

	fun showNoInternet() {
		//	gone(progressView)
		show(noInternet)
		show(dataView)
		swipe.showRefresh(false)
	}

	fun showDataWhenLoading() {
		//	gone(progressView)
		gone(noInternet)
		show(dataView)
		swipe.showRefresh(true)
	}

	/**
	 *
	 * выполнить проверку состояния загрузки
	 *
	 *@return вернёт true если данные в данный момент загружаются
	 */
	fun run(): Boolean {

		return if (isLoading()) {
			if (hasData())
				showDataWhenLoading()
			else
				showProgress()
			true
		} else {
			if (hasData())
				showData()
			else
				showNoInternet()
			false
		}
	}

	fun reload(): Boolean {
		startLoad()
		return run()
	}

}
