package com.picom.sixtysecond.inet

import android.view.View

class NoInternetSimple(

		private val progressView: View?,
		private val noInternet: View?,
		private val dataView: View?,
		private val isLoading: () -> Boolean,
		private val hasData: () -> Boolean,
		private val startLoad: () -> Unit
) {


	fun gone(view: View?) {
		view?.visibility = View.GONE
	}

	fun show(view: View?) {
		view?.visibility = View.VISIBLE
	}

	fun showData() {
		gone(progressView)
		gone(noInternet)
		show(dataView)
	}

	fun showProgress() {
		show(progressView)
		gone(noInternet)

	}

	fun showNoInternet() {
		gone(progressView)
		show(noInternet)
		show(dataView)
	}

	fun showDataWhenLoading() {
		gone(progressView)
		gone(noInternet)
		show(dataView)
	}

	/**
	 *
	 * выполнить проверку состояния загрузки
	 *
	 *@return вернёт true если данные в данный момент загружаются
	 */
	fun run(): Boolean {

		return if (isLoading()) {
			if (hasData())
				showDataWhenLoading()
			else
				showProgress()
			true
		} else {
			if (hasData())
				showData()
			else
				showNoInternet()
			false
		}
	}

	fun reload(): Boolean {
		startLoad()
		return run()
	}

}
