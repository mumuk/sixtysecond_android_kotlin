package com.picom.sixtysecond.bus

import com.squareup.otto.Bus
import java.util.*

object BusProvider {
	val bus = Bus()
	fun register(activity: Any) = bus.register(activity)
	fun unregister(activity: Any) = bus.unregister(activity)
	fun post(event: Any) = bus.post(event)
}