package com.picom.sixtysecond.bus.subscribers

class EndLoadUpdate(val infoType: String, val ids: Set<Int>)