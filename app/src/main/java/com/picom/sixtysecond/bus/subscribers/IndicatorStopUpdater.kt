package com.picom.sixtysecond.bus.subscribers

class IndicatorStopUpdater(val destination: String, val isStopped: Boolean) {
}