package com.picom.sixtysecond.bus

import com.picom.sixtysecond.bus.subscribers.ChangeSortUpdater
import com.picom.sixtysecond.bus.subscribers.EndLoadUpdate
import com.picom.sixtysecond.bus.subscribers.StartLoadUpdate

class BusConstants {
	companion object {
		fun changeSort(sortNapravlenie: Int) = ChangeSortUpdater(sortNapravlenie)

		fun startLoad(infoType: String, ids: Set<Int>): StartLoadUpdate = StartLoadUpdate(infoType, ids)
		fun endLoad(infoType: String, ids: Set<Int>): EndLoadUpdate = EndLoadUpdate(infoType, ids)

		fun score(ids: IntArray): ScoreUpdate = ScoreUpdate(ids)
		fun scoreDate(ids: IntArray): ScoreDateUpdate = ScoreDateUpdate(ids)
	}

	class ScoreUpdate(val ids: IntArray)
	class ScoreDateUpdate(val ids: IntArray)
}