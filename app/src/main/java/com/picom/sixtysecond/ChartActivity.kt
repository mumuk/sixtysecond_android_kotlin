package com.picom.sixtysecond

import android.os.Bundle
import com.picom.sixtysecond.fragments.ChartFragment
import com.picom.unionui.sview.SActivity
import com.picom.unionui.sview.SFragment

class ChartActivity : TActivity() {
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_chart)

		var tableIndex = 0
		var seasonName = 0
		var ids: IntArray? = null
		this.intent?.let {
			tableIndex = it.getIntExtra(SActivity.ARG_TABLE_NUMBER, 0)
			seasonName = it.getIntExtra(SActivity.ARG_TABLE_SEASON, 0)
			ids = it.getIntArrayExtra(SActivity.ARG_TABLE_IDS)
		}

		changeFragment(ChartFragment.newInstance(tableIndex, seasonName, ids), "chart")
	}

	fun changeFragment(fragment: SFragment, tag: String) {
		fragmentManager().beginTransaction().replace(R.id.navContainer, fragment, tag).commit()
	}

}