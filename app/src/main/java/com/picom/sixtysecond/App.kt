package com.picom.sixtysecond

import android.app.Application
import com.picom.sixtysecond.storage.DataStore

class App : Application() {
	override fun onCreate() {
		super.onCreate()
		DataStore.initialize(this.applicationContext)
	}
}