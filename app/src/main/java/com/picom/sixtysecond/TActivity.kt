package com.picom.sixtysecond

import android.annotation.SuppressLint
import android.content.Intent
import android.support.v7.app.ActionBar
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import com.picom.sixtysecond.bus.BusProvider
import com.picom.unionui.sview.SActivity

@SuppressLint("Registered")
open class TActivity : SActivity() {

	protected fun showBrowser(address: String, title: String) {
		val intent = Intent(this, BrowseActivity::class.java)
		intent.putExtra(TActivity.ADDRESS, address)
		intent.putExtra(TActivity.PARAM_TITLE, title)
		startActivity(intent)
	}

	protected fun addHomeBtn(title: String) {
		val toolbar: Toolbar? = find(R.id.toolbar)
		if (toolbar != null) {
			setSupportActionBar(toolbar)
			val actionBar: ActionBar? = supportActionBar
			if (actionBar != null) {
				actionBar.setDisplayHomeAsUpEnabled(true)
				actionBar.title = title
			}
		}
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		when (item.itemId) {
			android.R.id.home -> this.finish()
			else -> return super.onOptionsItemSelected(item)
		}
		return true
	}


	fun update() {

	}

	override fun onStart() {
		super.onStart()
		BusProvider.register(this)
		update()
	}

	override fun onStop() {
		BusProvider.unregister(this)
		super.onStop()
	}

	companion object {
		const val PARAM_LOCATION: String = "location"
		const val PARAM_LOCATION_ZOOM: String = "location_zoom"
		const val DEFAULT_MAP_ZOOM: Int = 16
		const val CITY_MAP_ZOOM: Int = 11
		const val STATE_MAP_POSITION: String = "state_map_position"
		const val ADDRESS = "address"
		const val PARAM_TITLE = "title"
		const val NUMBER_OF_REQUEST = 23401
	}
}