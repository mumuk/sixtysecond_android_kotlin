package com.picom.sixtysecond

import android.os.Bundle

class AboutActivity : TActivity() {
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_about)
		addHomeBtn(getString(R.string.titleAbout))
	}
}