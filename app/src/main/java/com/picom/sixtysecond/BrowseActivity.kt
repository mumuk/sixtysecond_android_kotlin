package com.picom.sixtysecond

import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient

class BrowseActivity : TActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_browse)
		val webView = find<WebView>(R.id.browse)


		webView?.webViewClient = CWebViewClient()
		var titleText = getString(R.string.titleBrowse)

		intent?.let {
			it.getStringExtra(TActivity.PARAM_TITLE)?.let { x -> titleText = x }
			it.getStringExtra(TActivity.ADDRESS)?.let { address -> webView?.loadUrl(address) }

		}

		addHomeBtn(titleText)
	}

	private class CWebViewClient : WebViewClient() {
		override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
			view.loadUrl(url)
			return true
		}
	}
}