package com.picom.sixtysecond.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.picom.sixtysecond.R
import com.picom.unionui.sview.SFragment

class AboutFragment : SFragment() {
	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		val view = inflater.inflate(R.layout.activity_about, container, false)
		addHomeBtn(view, R.id.toolbar, "", true)
		return view
	}
}