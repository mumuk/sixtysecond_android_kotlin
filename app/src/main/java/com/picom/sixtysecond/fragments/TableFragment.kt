package com.picom.sixtysecond.fragments

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.*
import com.picom.sixtysecond.ChartActivity
import com.picom.sixtysecond.R
import com.picom.sixtysecond.adapters.TableAdapter
import com.picom.sixtysecond.bus.subscribers.ChangeSortUpdater
import com.picom.sixtysecond.bus.subscribers.EndLoadUpdate
import com.picom.sixtysecond.bus.subscribers.StartLoadUpdate
import com.picom.sixtysecond.inet.NoInternet
import com.picom.sixtysecond.net.Program
import com.picom.sixtysecond.net.Program._DIVISION_A
import com.picom.sixtysecond.net.Program._DIVISION_B
import com.picom.sixtysecond.net.Program._SEASON_GLOBAL
import com.picom.sixtysecond.net.Program._SEASON_MATRIX
import com.picom.sixtysecond.storage.DataStore
import com.picom.unionui.sview.SActivity.Companion.ARG_TABLE_NUMBER
import com.picom.unionui.sview.SActivity.Companion.ARG_TABLE_SEASON
import com.picom.unionui.sview.SActivity.Companion.KEY_SAVED_FILTER_CONSTRAINT
import com.picom.unionui.sview.SActivity.Companion.KEY_SAVED_SELECTED_CONSTRAINT
import com.picom.unionui.sview.swipes.GoodSwipeList
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.header_table.*

class TableFragment : UiFragment(), View.OnClickListener {

	override val titleId: Int = R.string.titleTable

	var idTitle = 0
	var idSeason = 0


	private val swipe: GoodSwipeList by lazy { GoodSwipeList() }

	private var noIntenet: NoInternet? = null
	private var currentQuery = ""


	private val searchListener: android.support.v7.widget.SearchView.OnQueryTextListener =
			object : android.support.v7.widget.SearchView.OnQueryTextListener {
				fun changeFilter(query: String?) {
					query?.let {
						adapter.filter.filter(query)
						currentQuery = query
					}
				}

				override fun onQueryTextSubmit(query: String?): Boolean {
					changeFilter(query)
					return false
				}

				override fun onQueryTextChange(newText: String?): Boolean {
					changeFilter(newText)
					return true
				}
			}


	val adapter: TableAdapter by lazy {
		TableAdapter(this.activity!!, R.layout.item_table,
				reloadMethod = {
					if (noIntenet != null) {
						DataStore.reload(idTitle, idSeason)
						noIntenet?.run()
					}
				}
		)
	}

	override fun onStart() {
		super.onStart()
	}


	override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
		inflater.inflate(R.menu.table_menu, menu)
		val searchItem = menu.findItem(R.id.action_search)
		val searchView = searchItem.actionView as android.support.v7.widget.SearchView


		searchView.setOnQueryTextListener(searchListener)
		if (!currentQuery.isEmpty()) {
			val query = currentQuery
			searchItem.expandActionView()
			searchView.setQuery(query, true)
			searchView.clearFocus()
		}

	}

	override fun onSaveInstanceState(outState: Bundle) {
		super.onSaveInstanceState(outState)
		outState.putString(KEY_SAVED_FILTER_CONSTRAINT, currentQuery)
		outState.putIntArray(KEY_SAVED_SELECTED_CONSTRAINT, adapter.getSelectedIds())
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setHasOptionsMenu(true)
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		val view = inflater.inflate(R.layout.fragment_table, container, false)

		idTitle = arguments?.getInt(ARG_TABLE_NUMBER, 0) ?: 0
		idSeason = arguments?.getInt(ARG_TABLE_SEASON, 0) ?: 0


		val titleText = when (idSeason) {
			_SEASON_MATRIX -> getString(R.string.matrix)
			_SEASON_GLOBAL -> getString(R.string.global)
			else -> ""
		}


		val subTitle = when (idTitle) {
			_DIVISION_A -> getString(R.string.divisionA)
			_DIVISION_B -> getString(R.string.divisionB)
			else -> ""
		}

		addHomeBtn(view, R.id.toolbar, titleText, subTitle)
		updateTitle()


		swipe.initializeView(view, adapter, null)
		noIntenet = NoInternet(
				swipe,
				//view.findViewById(R.id.internetProgress),
				view.findViewById<View>(R.id.viewNoInternet),
				view.findViewById<View>(R.id.topLayout),
				isLoading = {
					DataStore.isLoadingTable(idTitle, idSeason)
				},
				hasData = {
					DataStore.getTable(idTitle, idSeason).size() > 0
				},
				startLoad = {
					DataStore.load(idTitle, idSeason)
				}
		)

		noIntenet?.reload()

		view.findViewById<View>(R.id.placeSortBtn).setOnClickListener(this)

		if (savedInstanceState != null) {
			currentQuery = savedInstanceState.getString(KEY_SAVED_FILTER_CONSTRAINT, "")
			adapter.setSelectedIds(savedInstanceState.getIntArray(KEY_SAVED_SELECTED_CONSTRAINT))
		}
		return view
	}

	override fun onResume() {
		super.onResume()
	}

	override fun onPause() {
		super.onPause()
	}

	fun updateTitle() {

		val dateTitle = DataStore.getTableTitle(idTitle, idSeason)

		(activity as AppCompatActivity).supportActionBar?.let { actionBar ->
			actionBar.setDisplayHomeAsUpEnabled(true)
			actionBar.subtitle = dateTitle.title + " " + dateTitle.getDate()
		}
	}

	@Subscribe
	fun endLoad(updater: EndLoadUpdate) {
		when (updater.infoType) {
			Program._API_MAIN -> {
				noIntenet?.run()
				updateSections()
			}
			Program._API_DATE -> {
				updateTitle()
			}
		}
	}

	@Subscribe
	fun startLoad(updater: StartLoadUpdate) {
		when (updater.infoType) {
			Program._API_MAIN -> {
				noIntenet?.run()
			}
		}
	}

	fun changeSort() {
		val sort = DataStore.sortNapravlenie
		adapter.changeSort(sort)
		when (sort) {
			DataStore.SORT_LESS -> tableHeaderTrade?.setImageResource(R.drawable.sort_rating_min)
			else -> tableHeaderTrade?.setImageResource(R.drawable.sort_rating)

		}
	}

	@Subscribe
	fun sortUpdate(sortUpdater: ChangeSortUpdater) {
		changeSort()
	}

	override fun update() {
		//swipe.update
		changeSort()

		noIntenet?.let {
			if (!it.run())
				updateSections()
		}

		updateTitle()
	}


	private fun updateSections() {
		adapter.changeArray(DataStore.getTable(idTitle, idSeason))

	}


	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		return when (item.itemId) {
			R.id.action_chart -> {
				showChart(idTitle, idSeason)
				true
			}
			else -> super.onOptionsItemSelected(item)
		}
	}

	override fun onClick(v: View) {
		when (v.id) {
			R.id.placeSortBtn ->
				DataStore.toggleSort()
			else -> {
			}
		}
	}

	fun showChart(idTitle: Int, idSeason: Int) {
		val ids = adapter.getSelectedIds()
		if (ids.isEmpty()) {
			swipe.list?.let { list ->
				Snackbar.make(list, R.string.errorSelectZeroCommand, Snackbar.LENGTH_SHORT).show()
			}
		} else {

			show(ChartActivity::class.java, idTitle, idSeason, ids)
		}
	}


	companion object {
		fun newInstance(indexTable: Int, seasonName: Int): TableFragment {
			val fragment = TableFragment()
			val args = Bundle()
			args.putInt(ARG_TABLE_NUMBER, indexTable)
			args.putInt(ARG_TABLE_SEASON, seasonName)
			fragment.arguments = args
			return fragment
		}
	}
}