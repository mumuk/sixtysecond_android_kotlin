package com.picom.sixtysecond.fragments

import android.content.Intent
import android.provider.SyncStateContract.Helpers.update
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.picom.sixtysecond.BrowseActivity
import com.picom.sixtysecond.R
import com.picom.sixtysecond.TActivity
import com.picom.sixtysecond.adapters.IConferenceAdapter
import com.picom.sixtysecond.bus.BusProvider
import com.picom.unionui.sview.SActivity
import com.picom.unionui.sview.SFragment

abstract class UiFragment : SFragment() {

	var pageNumber = 0

	abstract val titleId: Int //this abstract field

	fun initialize() {

		arguments?.let {
			pageNumber = it.getInt(SActivity.ARGUMENT_PAGE_NUMBER)
		}
	}

	override fun onStop() {
		BusProvider.unregister(this)
		super.onStop()
	}

	override fun onStart() {
		super.onStart()
		BusProvider.register(this)
		restoreTitle()
		update()
	}

	fun <T : RecyclerView.ViewHolder> createList(view: View, adapter: IConferenceAdapter<T>): RecyclerView? {
		val list: RecyclerView? = find(view, R.id.list)

		list?.let {
			it.adapter = adapter
			val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
			it.layoutManager = layoutManager
			it.addItemDecoration(DividerItemDecoration(this.activity, DividerItemDecoration.VERTICAL))
		}
		return list
	}

	abstract fun update() //this is abstract method

	fun restoreTitle() {
		activity?.setTitle(titleId)
	}

	protected fun showBrowser(address: String, title: String) {
		val intent = Intent(this.activity, BrowseActivity::class.java)
		intent.putExtra(TActivity.ADDRESS, address)
		intent.putExtra(TActivity.PARAM_TITLE, title)
		startActivity(intent)
	}
}