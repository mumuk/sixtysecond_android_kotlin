package com.picom.sixtysecond.fragments

import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.github.mikephil.charting.listener.ChartTouchListener
import com.github.mikephil.charting.listener.OnChartGestureListener
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.picom.sixtysecond.R
import com.picom.sixtysecond.bus.subscribers.EndLoadUpdate
import com.picom.sixtysecond.bus.subscribers.StartLoadUpdate
import com.picom.sixtysecond.inet.NoInternetSimple
import com.picom.sixtysecond.model.NullEntity
import com.picom.sixtysecond.net.Program
import com.picom.sixtysecond.storage.DataStore
import com.picom.unionui.sview.SActivity.Companion.ARG_TABLE_IDS
import com.picom.unionui.sview.SActivity.Companion.ARG_TABLE_NUMBER
import com.picom.unionui.sview.SActivity.Companion.ARG_TABLE_SEASON
import com.squareup.otto.Subscribe
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.formatter.IAxisValueFormatter


class ChartFragment : UiFragment(), OnChartGestureListener, OnChartValueSelectedListener {

//	val colors = arrayOf(
//			0xFF800080,
//			0xFF0000FF,
//			0xFF008000,
//			0xFFFF0000,
//			0xFF808080,
//			0xFF7CFC00,
//			0xFFA0522D,
//			0xFF9ACD32,
//			0xFF8B0000
//	)

	override val titleId: Int = R.string.titleChart

	private val dp: Float by lazy { activity?.resources?.displayMetrics?.density ?: 1f }

	private var chart: LineChart? = null
	private lateinit var noIntenet: NoInternetSimple
	private var chartData = NullEntity.ChartModel
	private var idTitle = 0
	private var idSeason = 0
	private var ids = intArrayOf()


	fun generateColor(index: Int): Int {
		val h = (((5 * index) % 12) * 360 / 12f) % 360
		val v = 1f - ((index / 12) % 4) * 0.2f
		val arr = floatArrayOf(h, 1f, v)
		return Color.HSVToColor(arr)

	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		val view = inflater.inflate(R.layout.fragment_chart, container, false)

		idTitle = arguments?.getInt(ARG_TABLE_NUMBER, 0) ?: 0
		idSeason = arguments?.getInt(ARG_TABLE_SEASON, 0) ?: 0
		ids = arguments?.getIntArray(ARG_TABLE_IDS) ?: intArrayOf()



		addHomeBtn(view, R.id.toolbar, getString(titleId), false)

		noIntenet = NoInternetSimple(
				view.findViewById<View>(R.id.internetProgress),
				null,
				view.findViewById<View>(R.id.chart),
				isLoading = {
					DataStore.isLoadingChart(idTitle, idSeason)
				},
				hasData = {
					DataStore.getChart(idTitle, idSeason).size() > 0
				},
				startLoad = {
					DataStore.loadChart(idTitle, idSeason)
				}
		)

		noIntenet.reload()

		return view
	}

	@Subscribe
	fun endLoad(updater: EndLoadUpdate) {

		when (updater.infoType) {
			Program._API_CHART -> {
				noIntenet.run()
				updateSections()
			}
		}
	}

	override fun update() {
		if (!noIntenet.run())
			updateSections()

	}


	private fun updateSections() {
		chartData = DataStore.getChart(idTitle, idSeason)
		recreateChart()

		(activity as AppCompatActivity).supportActionBar?.let { actionBar ->
			actionBar.title = chartData.title

		}
	}


	@Subscribe
	fun startLoad(updater: StartLoadUpdate) {
		if (updater.infoType == Program._API_MAIN) {
			noIntenet.run()
		}


	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		chart = find<LineChart>(view, R.id.chart)
		chart?.let { mChart ->

			mChart.onChartGestureListener = this
			mChart.setOnChartValueSelectedListener(this)
			mChart.setDrawGridBackground(true)
			mChart.setPinchZoom(true)

			val descr = Description()
			descr.text = ""
			mChart.description = descr
			mChart.setNoDataText(getString(R.string.chartNoDataText))


			with(mChart.legend) {
				isWordWrapEnabled = true
				form = Legend.LegendForm.LINE
				horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
				verticalAlignment = Legend.LegendVerticalAlignment.TOP
				orientation = Legend.LegendOrientation.VERTICAL
			}

			//Ось y
			val leftAxis = mChart.axisLeft
			with(leftAxis) {
				removeAllLimitLines()
				axisMinimum = 300f
				axisMinimum = 0f
				enableGridDashedLine(10f, 10f, 0f)
				setDrawZeroLine(false)
				setDrawLimitLinesBehindData(true)
			}

			mChart.axisRight.isEnabled = false
			mChart.animateX(2500, Easing.EasingOption.EaseInOutQuart)

		}

	}

	fun recreateChart() {

		chart?.let { mChart ->

			val minValue: Float = chartData.minValue() - 1
			val maxValue: Float = chartData.maxValue() + 1

			val leftAxis = mChart.axisLeft
			with(leftAxis) {
				removeAllLimitLines()
				axisMaximum = maxValue
				axisMinimum = minValue
				setDrawZeroLine(false)
				isEnabled = false
			}

			val rightAxis = mChart.axisRight

			val legend = mChart.legend
			with(legend) {
				form = Legend.LegendForm.LINE
				horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
				verticalAlignment = Legend.LegendVerticalAlignment.TOP
				orientation = Legend.LegendOrientation.VERTICAL
				setDrawInside(true)
			}


			//=================================

			val dataSets = ArrayList<ILineDataSet>()
			val xVals = chartData.labels

			var maxValuen = 10f

			var jj = 0
			for (jid in ids) {
				(chartData.series.find { it.getId() == jid })?.let {
					val serie = it
					val color = generateColor(jj) // colors(jj%9)
					jj += 1

					val yVals = ArrayList<Entry>()
					val series = serie.series
					for (i in series.indices) {
						val si = series[i]
						if (si > maxValuen)
							maxValuen = si
						yVals.add(Entry(i.toFloat(), si))
					}

					val set1 = LineDataSet(yVals, serie.name)
					with(set1) {
						set1.color = color
						setCircleColor(color)
						lineWidth = 1f * dp
						circleRadius = 1f * dp
						setDrawCircleHole(false)
						valueTextSize = 8f * dp
						valueTextColor = 0
						setDrawFilled(false)
						setDrawCircles(false)
						//setDrawStepped(false)
						setDrawCircleHole(false)
					}

					dataSets.add(set1)

				}
			} //end for j


			maxValuen += 1

			rightAxis.isEnabled = true
			rightAxis.axisMaximum = maxValuen
			leftAxis.axisMaximum = maxValuen
			rightAxis.axisMinimum = minValue

			val formatter = IAxisValueFormatter { value, axis -> xVals.getOrElse(value.toInt()) { "" } }

			val xAxis = mChart.xAxis
			xAxis.granularity = 1f // minimum axis-step (interval) is 1
			xAxis.valueFormatter = formatter

			val data = LineData(dataSets)
			mChart.data = data
		}
	}


	override fun onChartFling(me1: MotionEvent, me2: MotionEvent, velocityX: Float, velocityY: Float) {

	}

	override fun onChartScale(me: MotionEvent, scaleX: Float, scaleY: Float) {

	}

	override fun onChartDoubleTapped(me: MotionEvent) {

	}

	override fun onChartGestureEnd(me: MotionEvent, lastPerformedGesture: ChartTouchListener.ChartGesture) {

	}

	override fun onChartTranslate(me: MotionEvent, dX: Float, dY: Float) {

	}

	override fun onChartLongPressed(me: MotionEvent) {

	}

	override fun onChartGestureStart(me: MotionEvent, lastPerformedGesture: ChartTouchListener.ChartGesture) {

	}

	override fun onChartSingleTapped(me: MotionEvent) {

	}

	override fun onNothingSelected() {

	}

	override fun onValueSelected(e: Entry?, h: Highlight?) {
		//mChart?.centerViewToAnimated(e.getXIndex(), e.getVal(), mChart?.getData()?.getDataSetByIndex(dataSetIndex).getAxisDependency(), 500);
	}

	companion object {
		fun newInstance(indexTable: Int, seasonName: Int, ids: IntArray?): ChartFragment {
			val fragment = ChartFragment()
			val args = Bundle()
			args.putInt(ARG_TABLE_NUMBER, indexTable)
			args.putInt(ARG_TABLE_SEASON, seasonName)
			args.putIntArray(ARG_TABLE_IDS, ids)
			fragment.arguments = args
			return fragment
		}
	}
}