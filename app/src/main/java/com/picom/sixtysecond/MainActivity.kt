package com.picom.sixtysecond

import android.content.Context
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.view.MenuItem
import com.picom.sixtysecond.fragments.TableFragment
import com.picom.sixtysecond.net.Program._DIVISION_A
import com.picom.sixtysecond.net.Program._DIVISION_B
import com.picom.sixtysecond.net.Program._SEASON_GLOBAL
import com.picom.sixtysecond.net.Program._SEASON_MATRIX
import com.picom.unionui.sview.SFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : TActivity(), NavigationView.OnNavigationItemSelectedListener {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)


		navigationView.setNavigationItemSelectedListener(this)
		drawerLayout.addDrawerListener(object : DrawerLayout.SimpleDrawerListener() {
			override fun onDrawerStateChanged(newState: Int) {
				hideKeyboard()
			}
		})


		if (fragmentInContainer() == null) {
			val sp = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
			val tab = sp.getInt(KEY_TABLE, -1)
			val sea = sp.getInt(KEY_SEASON, -1)

			if (tab >= 0 && sea >= 0) {
				showTable(tab, sea)
			} else {
				showMain()
				drawerLayout.openDrawer(GravityCompat.START)
			}
		}
	}


	override fun onBackPressed() {
		//	val drawer: DrawerLayout = findViewById(R.id.drawer_layout).asInstanceOf[DrawerLayout]
		if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
			drawerLayout.closeDrawer(GravityCompat.START)
		} else {
			when (fragmentInContainer()) {
				is TableFragment -> super.onBackPressed()
				else -> showMain()
			}
		}
	}

	fun fragmentInContainer() = fragmentManager().findFragmentById(R.id.navContainer)

	fun changeFragment(fragment: SFragment, tag: String) {
		fragmentManager().beginTransaction().replace(R.id.navContainer, fragment, tag).commit()
	}


	override fun onNavigationItemSelected(item: MenuItem): Boolean {
		when (item.itemId) {
			R.id.nav_about -> showAbout()
			R.id.nav_commandA_matrix -> showTable(_DIVISION_A, _SEASON_MATRIX)
			R.id.nav_commandA_global -> showTable(_DIVISION_A, _SEASON_GLOBAL)
			R.id.nav_commandB_matrix -> showTable(_DIVISION_B, _SEASON_MATRIX)
			R.id.nav_commandB_global -> showTable(_DIVISION_B, _SEASON_GLOBAL)
			else -> {
			}
		}

		drawerLayout.closeDrawer(GravityCompat.START)

		return true
	}

	private fun selectNavigation(tableIndex: Int, seasonName: Int): Boolean {

		val index = when (Pair(tableIndex, seasonName)) {
			Pair(_DIVISION_A, _SEASON_MATRIX) -> R.id.nav_commandA_matrix
			Pair(_DIVISION_A, _SEASON_GLOBAL) -> R.id.nav_commandA_global
			Pair(_DIVISION_B, _SEASON_MATRIX) -> R.id.nav_commandB_matrix
			Pair(_DIVISION_B, _SEASON_GLOBAL) -> R.id.nav_commandB_global
			else -> 0
		}

		return if (index != 0) {
			navigationView.setCheckedItem(index)
			true
		} else
			false
	}

	fun saveSelect(tableIndex: Int, seasonName: Int) {
		val sp = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
		val edit = sp.edit()
		edit.putInt(KEY_TABLE, tableIndex)
		edit.putInt(KEY_SEASON, seasonName)
		edit.apply()

	}

	fun showTable(tableIndex: Int, seasonName: Int) {
		selectNavigation(tableIndex, seasonName)
		saveSelect(tableIndex, seasonName)
		changeFragment(TableFragment.newInstance(tableIndex, seasonName), "main_$tableIndex")
	}

	fun showMain() {
		when (fragmentInContainer()) {
			is TableFragment -> {
			}
			else -> showTable(FIRST_DIVSION_ID, FIRST_SEASON_ID)
		}
	}

	fun showAbout() {
		show(AboutActivity::class.java)
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {

		return when (item.itemId) {
			android.R.id.home -> {
				drawerLayout.openDrawer(GravityCompat.START)
				true
			}
			else -> super.onOptionsItemSelected(item)
		}
	}

	companion object {
		const val PREF_NAME = "StartPref"
		const val KEY_TABLE = "select_table"
		const val KEY_SEASON = "select_season"

		const val FIRST_DIVSION_ID = _DIVISION_A
		const val FIRST_SEASON_ID = _SEASON_GLOBAL
	}
}
