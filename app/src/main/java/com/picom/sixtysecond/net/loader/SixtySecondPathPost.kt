package com.picom.sixtysecond.net.loader

import com.picom.sixtysecond.common.net.*
import com.picom.sixtysecond.net.Program
import okhttp3.RequestBody

class SixtySecondPathPost(val context: IContexter,
						  override val jsonConstructor: IJSONConstructor,
						  val arguments: GoodPostArguments,
						  val ids: Set<Int>)
	: IJSONPoster {

	override fun usedCookies(): Boolean = false

	override fun generatePost(): RequestBody {
		val pair = TPostListNameValuePair()
		if (ids.isNotEmpty()) {
			val num = ids.first()

			pair.add(Program._API_MAIN, num % Program._SEASON_MULT)
			pair.add(Program._ACTION_KEY, num / Program._SEASON_MULT)
		}

		return pair.encodePost()
	}

	override val method = EMethod.post

	override fun generateGet(): String = ""

	override fun construct(json: TJSONObject?, info: IContexter) {
		if (json != null) {
			if (this.jsonConstructor != null) {
				this.jsonConstructor.construct(info, json, true)
			}
		}
	}

	/**
	 * Выполнится перед запросом к серверу
	 *
	 * @return
	 */
	override fun predZapros(): Boolean = ids.isNotEmpty()

	/**
	 * Выполнится только если запрос к серверу был (то есть если predZapros true)
	 */
	override fun postZapros() {
		arguments.endLoad()
	}

	/**
	 * Выполнится только если запрос к серверу не произойдёт (то есть если predZapros false)
	 */
	override fun cancelZapros() {

	}
}