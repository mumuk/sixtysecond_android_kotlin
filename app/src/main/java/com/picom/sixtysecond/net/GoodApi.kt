package com.picom.sixtysecond.net

import com.picom.sixtysecond.common.net.EConstructType
import com.picom.sixtysecond.common.net.GoodPostArguments
import com.picom.sixtysecond.model.ChartModel
import com.picom.sixtysecond.model.ScoreDateModel
import com.picom.sixtysecond.model.TableModel
import com.picom.sixtysecond.net.Program._API_URL
import com.picom.sixtysecond.net.Program._API_URL_CHART
import com.picom.sixtysecond.net.Program._API_URL_DATE
import com.picom.sixtysecond.net.constructors.ChartConstructor
import com.picom.sixtysecond.net.constructors.TableScoreConstructor
import com.picom.sixtysecond.net.constructors.TableScoreDateConstructor
import com.picom.sixtysecond.net.loader.SixtySecondBasesLoader
import com.picom.sixtysecond.storage.DataStore
import com.picom.sixtysecond.storage.collection.ListLoadingData
import java.util.concurrent.Executors


class GoodApi {
	companion object {

		private val tpe = Executors.newSingleThreadExecutor()


		fun loadTableDate(listData: ListLoadingData<ScoreDateModel>, ids: Set<Int>) {
			//this method don't have body
		}

		fun loadTable(listData: ListLoadingData<TableModel>, ids: Set<Int>) {
			val arguments = GoodPostArguments(Program._API_MAIN, "", ids,
					EConstructType.arrayAction,
					{ listData.start(it) }, { listData.end(it) })

			SixtySecondBasesLoader(
					DataStore.info,
					_API_URL,
					TableScoreConstructor(arguments, listData),
					arguments,
					false, true, false
			).executeOnExecutor(tpe)

			val argumentsDate = GoodPostArguments(Program._API_DATE, "", ids,
					EConstructType.arrayAction,
					{ DataStore.getScoreDates().start(it) }, { DataStore.getScoreDates().end(it) })

			SixtySecondBasesLoader(
					DataStore.info,
					_API_URL_DATE,
					TableScoreDateConstructor(argumentsDate, DataStore.getScoreDates()),
					argumentsDate,
					false, true, false
			).executeOnExecutor(tpe)
		}

		fun loadChart(listData: ListLoadingData<ChartModel>, ids: Set<Int>) {
			val arguments = GoodPostArguments(Program._API_CHART, "", ids,
					EConstructType.arrayAction,
					{ listData.start(it) }, { listData.end(it) })


			SixtySecondBasesLoader(
					DataStore.info,
					_API_URL_CHART,
					ChartConstructor(arguments, listData),
					arguments,
					false, true, false
			).executeOnExecutor(tpe)
		}


	}
}