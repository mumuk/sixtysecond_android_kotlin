package com.picom.sixtysecond.net.loader

import android.content.ContentValues
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.provider.BaseColumns._ID
import com.picom.sixtysecond.common.net.IContexter
import com.picom.sixtysecond.storage.db.DataColumns.COLUMN_MODTIME
import com.picom.sixtysecond.storage.db.DataColumns.COLUMN_VALUE
import com.picom.sixtysecond.storage.db.DataColumns.projection
import org.json.JSONArray
import org.json.JSONObject

object EventSaver {
	fun loadSonTable(tableName: String, context: IContexter?, column: Int): JSONArray? {
		if (context == null) return null

		var otvet: JSONArray? = null
		try {
			getReadableDB(context)?.use { db ->
				val values: Array<String> = arrayOf(column.toString())
				val cursorQuery = db.query(
						tableName,
						projection,
						"$_ID = ?",
						values,
						null,
						null,
						null)

				cursorQuery.use { cursor ->
					if (cursor.moveToFirst()) {
						val jsonText = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_VALUE))
						otvet = JSONArray(jsonText)
					}
				}
			}
		} catch (ignored: Exception) {

		}
		return otvet
	}

	fun getReadableDB(context: IContexter): SQLiteDatabase? = context.getDB()

	fun toJSONObject(jsonArray: JSONArray): JSONObject {
		val obj = JSONObject()
		obj.put("datas", jsonArray)
		return obj
	}


	private fun saveSonArr(context: IContexter?, column: Long, tableName: String, obj: String?) {
		if (context == null || obj == null) return

		try {
			context.getWritableDB()?.use { db ->
				val values = ContentValues()
				val time: Long = System.currentTimeMillis()
				values.put(_ID, column)
				values.put(COLUMN_VALUE, obj)
				values.put(COLUMN_MODTIME, time)
				val newRowId = db.insertWithOnConflict(
						tableName,
						null,
						values,
						SQLiteDatabase.CONFLICT_REPLACE)
			}
		} catch (ignored: Exception) {

		}
	}

	fun saveSon(context: IContexter, id: Int, tableName: String, arr: JSONArray) {
		saveSonArr(context, id.toLong(), context.tableName(tableName), arr.toString())
	}

}