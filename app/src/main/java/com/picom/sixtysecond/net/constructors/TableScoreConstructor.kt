package com.picom.sixtysecond.net.constructors

import com.picom.sixtysecond.common.net.GoodPostArguments
import com.picom.sixtysecond.common.net.IContexter
import com.picom.sixtysecond.common.net.IJSONConstructor
import com.picom.sixtysecond.common.net.TJSONObject
import com.picom.sixtysecond.model.TableModel
import com.picom.sixtysecond.net.Program
import com.picom.sixtysecond.net.loader.EventSaver
import com.picom.sixtysecond.storage.collection.ListLoadingData


class TableScoreConstructor(
		val arguments: GoodPostArguments,
		val listData: ListLoadingData<TableModel>) : IJSONConstructor {
	override fun construct(info: IContexter, tjsonObject: TJSONObject, needSave: Boolean) {

		if (tjsonObject == null)
			return



		when (arguments.infoType) {
			Program._API_MAIN ->
				if (arguments.ids.isNotEmpty()) {
					val arr = tjsonObject.jsonArray
					if (arr != null) {
						val id = arguments.ids.first()
						val table = TableModel(id, arr)
						if (needSave) {
							EventSaver.saveSon(info, id, arguments.infoType, arr)
						}
						info.newElement(listData.push(arrayOf(table), true))
					} //end if arr
				}
			else -> {
			}
		}

	}
}