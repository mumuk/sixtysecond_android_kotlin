package com.picom.sixtysecond.net


/**
 * Created by Kos on 10.03.2016.
 */
object Program {
	const val _API_URL = "http://60sec.picom.su/api/index.php"
	const val _API_URL_DATE = "http://60sec.picom.su/api/date.php"
	const val _API_URL_CHART = "http://60sec.picom.su/api/graph.php"

	const val _API_EVENT = "Event"
	const val _API_PLACE = "Place"

	const val _API_MAIN = "contest"
	const val _API_CHART = "chart"
	const val _API_DATE = "date"

	const val _ACTION_KEY = "season"
	const val _ACTION_EMPTY = ""

	const val _SEASON_MULT = 10000

	const val _SEASON_MATRIX = 2
	const val _SEASON_GLOBAL = 1

	const val _DIVISION_A = 3
	const val _DIVISION_B = 4
	const val _DIVISION_COMP = 5

	fun getTableId(idTitle: Int, idSeason: Int): Int {
		return idTitle * _SEASON_MULT + idSeason
	}

	fun getChartId(idTitle: Int, idSeason: Int): Int {
		return idTitle * 10 + idSeason
	}

	fun convertChartIdToId(chartType: Int): Int {
		val idSeason = chartType % 10
		val idTitle = chartType / 10
		return getTableId(idTitle, idSeason)
	}

}
