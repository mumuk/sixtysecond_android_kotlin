package com.picom.sixtysecond.net.loader

import android.annotation.TargetApi
import android.os.AsyncTask
import android.os.Build
import com.picom.sixtysecond.common.net.*

import org.json.JSONArray


@TargetApi(Build.VERSION_CODES.CUPCAKE)
class SixtySecondBasesLoader(private val info: IContexter, private val url: String, private val constructor: IJSONConstructor, private val arguments: GoodPostArguments,
							 private val onlyServer: Boolean,
							 private val defearDownload: Boolean,
							 private val onlyBase: Boolean) : AsyncTask<String, Void, Boolean>() {

	override fun onPreExecute() {
		arguments.startLoad()
		super.onPreExecute()
	}

	override fun doInBackground(vararg urls: String): Boolean {
		return background(info, url, constructor, arguments, onlyServer, defearDownload, onlyBase)
	}

	private fun background(info: IContexter, url: String, constructor: IJSONConstructor, arguments: GoodPostArguments, onlyServer: Boolean, defearDownload: Boolean, onlyBase: Boolean): Boolean {
		if (arguments.ids.isEmpty())
			return true


		if (onlyServer) {
			return asynh(constructor, info, url, arguments, arguments.ids)
		} else {

			val obj: JSONArray? =
					EventSaver.loadSonTable(info.tableName(arguments.infoType), info, arguments.ids.first())

			constructor.construct(info.info(), TJSONObject(obj), false)

			if (!onlyBase) {
				val needUpdate =
						if (defearDownload) arguments.ids
						else {
							if (obj != null) {
								arguments.ids.asSequence().drop(1).toSet()
							} else
								arguments.ids
						}
				if (needUpdate.isNotEmpty()) {
					return asynh(constructor, info, url, arguments, needUpdate) // needUpdate.map(x ⇒ x._2))
				}
			}
		}
		return false

	}

	private fun asynh(constructor: IJSONConstructor, info: IContexter,
					  url: String, arguments: GoodPostArguments, ids: Set<Int>): Boolean {
		return when (arguments.constructType) {
			EConstructType.arrayAction -> JSONRequest.asynhArray(info.info(), url, SixtySecondPathPost(info, constructor, arguments, ids))
			else -> JSONRequest.asynh(info.info(), url, SixtySecondPathPost(info, constructor, arguments, ids))
		}

	}

	/**
	 *
	 * @param result если результат false значит загрузка окончена и мы должны их извлечь из загрузки
	 */
	override fun onPostExecute(result: Boolean) {
		super.onPostExecute(result)

		this.info.push()

		if ((!result)) {
			arguments.endLoad()
		}
	}
}
