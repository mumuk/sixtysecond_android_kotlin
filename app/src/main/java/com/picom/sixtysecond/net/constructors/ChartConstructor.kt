package com.picom.sixtysecond.net.constructors

import com.picom.sixtysecond.common.SON
import com.picom.sixtysecond.common.net.GoodPostArguments
import com.picom.sixtysecond.common.net.IContexter
import com.picom.sixtysecond.common.net.IJSONConstructor
import com.picom.sixtysecond.common.net.TJSONObject
import com.picom.sixtysecond.model.ChartModel
import com.picom.sixtysecond.net.Program
import com.picom.sixtysecond.net.loader.EventSaver
import com.picom.sixtysecond.storage.collection.ListLoadingData


class ChartConstructor(
		val arguments: GoodPostArguments,
		val listData: ListLoadingData<ChartModel>) : IJSONConstructor {

	override fun construct(info: IContexter, tjsonObject: TJSONObject, needSave: Boolean) {

		if (tjsonObject == null)
			return

		when (arguments.infoType) {
			Program._API_CHART ->


				if (arguments.ids.isNotEmpty()) {
					val arr = tjsonObject.jsonArray
					if (arr != null) {

						val s = ArrayList<ChartModel>()
						for (i in 0 until arr.length()) {
							val obj = arr.optJSONObject(i)
							var oid = 0
							oid = SON.v(obj, "type", oid)


							val v = Program.convertChartIdToId(oid)
							val table = ChartModel(obj, v)
							s += table
						}
						if (arguments.ids.isNotEmpty()) {
							val id = arguments.ids.first()

							if (needSave) {
								EventSaver.saveSon(info, id, arguments.infoType, arr)
							}
						}
						info.newElement(listData.push(s.toTypedArray(), true))
					} //end if arr
				}
			else -> {
			}

		}


	}
}