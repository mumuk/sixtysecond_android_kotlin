package com.picom.sixtysecond.net.constructors

import com.picom.sixtysecond.common.net.GoodPostArguments
import com.picom.sixtysecond.common.net.IContexter
import com.picom.sixtysecond.common.net.IJSONConstructor
import com.picom.sixtysecond.common.net.TJSONObject
import com.picom.sixtysecond.model.ScoreDateModel
import com.picom.sixtysecond.net.Program
import com.picom.sixtysecond.net.loader.EventSaver
import com.picom.sixtysecond.storage.collection.ListLoadingData

class TableScoreDateConstructor(val arguments: GoodPostArguments,
								val listData: ListLoadingData<ScoreDateModel>) : IJSONConstructor {

	override fun construct(info: IContexter, tjsonObject: TJSONObject, needSave: Boolean) {

		if (tjsonObject == null)
			return

		when (arguments.infoType) {

			Program._API_DATE ->
				if (arguments.ids.isNotEmpty()) {
					val arr = tjsonObject.jsonArray
					if (arr != null) {
						val id = arguments.ids.first()
						val dateObj = arr.optJSONObject(0)
						val tableDate = ScoreDateModel(dateObj, id)

						if (needSave) {
							EventSaver.saveSon(info, id, arguments.infoType, arr)
						}
						info.newElement(listData.push(arrayOf(tableDate), true))
					} //end if arr
				}
			else -> {
			}

		}


	}
}