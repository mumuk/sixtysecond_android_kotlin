package com.picom.sixtysecond.model

import com.picom.sixtysecond.common.SON
import org.json.JSONObject


open class ChartModel() : BaseModel() {

	fun minValue(): Float =
			if (series.isNotEmpty()) {
				series.map { it.min() }.min() ?: 0f
			} else
				0f

	fun maxValue() =
			if (series.isNotEmpty()) {
				series.map { it.max() }.max() ?: 100f
			} else {
				100f
			}


	fun size() = labels.size

	private var typep = 0
	var title = ""
		private set

	var labels = Array(0) { "" }
		private set

	var series = ArrayList<ChartSeriesModel>()
		private set

	constructor(obj: JSONObject?) : this() {
		if (obj != null) {
			baseSON(obj)
		}
	}

	constructor(obj: JSONObject?, id: Int) : this(obj) {
		this.mId = id
	}

	override fun baseSON(obj: JSONObject) {
		super.baseSON(obj)

		mId = SON.v(obj, "type", mId)
		title = SON.v(obj, "title", title)
		labels = SON.getStringList(obj, "labels")
		series = SON.getMap(obj, "series") { objct, className -> ChartSeriesModel(objct) }
	}

	fun getName() = title


}
