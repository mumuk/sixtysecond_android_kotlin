package com.picom.sixtysecond.model

import com.picom.sixtysecond.common.SON
import org.json.JSONObject

open class ScoreDateModel() : BaseModel() {

	private var dateText = ""
	var title = ""
		private set


	constructor(obj: JSONObject?) : this() {

		if (null != obj) {
			baseSON(obj)
		}
	}

	constructor(obj: JSONObject?, id: Int) : this(obj) {
		this.mId = id
	}

	override fun baseSON(obj: JSONObject) {
		super.baseSON(obj)
		dateText = SON.v(obj, "date", dateText)
		title = SON.v(obj, "title", title)

	}

	fun getDate() = dateText
//	fun getTitle() = title
}