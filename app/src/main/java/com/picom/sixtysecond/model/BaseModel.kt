package com.picom.sixtysecond.model


import com.picom.sixtysecond.common.SON
import com.picom.sixtysecond.storage.collection.IEntity

import org.json.JSONObject


open class BaseModel : Comparable<BaseModel>, IEntity, IData {

	override fun getId(): Int = mId


	var sort = 0
		// Маркер сортировки
		protected set

	protected var mId = 0// идентификатор текущего

	constructor() {}

	constructor(obj: JSONObject?) {
		if (null == obj) return
		baseSON(obj)
	}


	open fun baseSON(obj: JSONObject) {
		sort = SON.v(obj, _sort, sort)
		mId = SON.v(obj, _id, mId)
	}

	override fun compareTo(another: BaseModel): Int {
		return if (sort < another.sort) -1 else if (sort == another.sort) 0 else 1
	}

	override fun isNull(): Boolean {
		return false
	}

	override fun handle() {

	}

	companion object {
		const val _sort = "sort"
		const val _id = "id"
	}
}

