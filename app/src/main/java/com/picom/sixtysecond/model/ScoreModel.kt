package com.picom.sixtysecond.model

import com.picom.sixtysecond.common.SON
import org.json.JSONObject
import java.text.DecimalFormat

open class ScoreModel() : ItemModel() {

	fun placeTop(topPlace: Int): Boolean {
		var r = 0
		for (c in place) {
			when (c) {
				in '0'..'9' -> r = r * 10 + (c - '0')
				else -> return r != 0 && r <= topPlace
			}
		}
		return r != 0 && r <= topPlace
	}


	var score: Float = 0f
		protected set

	var place: String = ""
		private set

	protected var delta: Int = 0

	fun scoreText() = decimalFormal.format(score)
	fun getTrade() = delta


	constructor(idV: Int, titleV: String, scoreV: Float, delta: Int, placeV: String) : this() {

		this.mId = idV
		this.name = titleV
		this.score = scoreV
		this.place = placeV
		this.delta = delta


	}

	constructor(obj: JSONObject?) : this() {
		if (null != obj) {
			baseSON(obj)
		}
	}


	override fun baseSON(obj: JSONObject) {
		super.baseSON(obj)
		place = SON.v(obj, "place", place)
		delta = SON.v(obj, "delta", delta)
		score = SON.v(obj, "ratio", score)

	}

	companion object {
		val decimalFormal = DecimalFormat("#.##")
	}
}