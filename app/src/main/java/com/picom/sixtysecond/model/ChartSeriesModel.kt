package com.picom.sixtysecond.model

import com.picom.sixtysecond.common.SON
import org.json.JSONObject


class ChartSeriesModel() : BaseItemModel() {

	fun min(): Float = series.min() ?: 0f
	fun max(): Float = series.max() ?: 100f

	var series = FloatArray(0)
		private set

	constructor(obj: JSONObject?) : this() {
		if (obj != null) {
			baseSON(obj)
		}
	}

	override fun baseSON(obj: JSONObject) {
		super.baseSON(obj)
		series = SON.getFloatList(obj, "series")
	}
}