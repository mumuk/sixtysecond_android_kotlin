package com.picom.sixtysecond.model

object NullEntity {

	val ChartModel: ChartModel = NoneChart()

	val Score: ScoreModel = NoneScore()
	val TableScore = NoneTableScore()
	val ScoreDate = NoneScoreDate()
}


class NoneTableScore : TableModel() {
	override fun isNull() = true
}

class NoneScoreDate : ScoreDateModel() {
	override fun isNull() = true
}

class NoneChart : ChartModel() {
	override fun isNull() = true
}

class NoneScore : ScoreModel() {
	override fun isNull() = true
}
