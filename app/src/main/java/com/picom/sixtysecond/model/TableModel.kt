package com.picom.sixtysecond.model

import org.json.JSONArray
import org.json.JSONObject

open class TableModel() : BaseModel() {


	var scores = ArrayList<ScoreModel>()

	fun size() = scores.size

	constructor(id: Int, arr: JSONArray?) : this() {

		this.mId = id
		if (arr != null) {
			val b = ArrayList<ScoreModel>()
			for (i in 0 until arr.length()) {
				b += ScoreModel(arr.optJSONObject(i))
			}


			b.sortBy { it.sort }
			scores = b
		}
	}

	constructor(obj: JSONObject?) : this() {

		if (obj != null) {
			baseSON(obj)
		}
	}

	override fun baseSON(obj: JSONObject) {
		super.baseSON(obj)
	}
}