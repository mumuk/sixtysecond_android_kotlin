package com.picom.sixtysecond.model


import com.picom.sixtysecond.common.SON

import org.json.JSONObject

open class ItemModel : BaseItemModel {

	var image = ""
		// Картинка эвента для Preview
		private set

	constructor() {}

	constructor(obj: JSONObject?) {
		if (null == obj) return
		baseSON(obj)
	}

	constructor(obj: JSONObject?, id: Int) {
		if (null == obj) return
		baseSON(obj)
		this.mId = id

	}


	override fun baseSON(obj: JSONObject) {
		super.baseSON(obj)
		image = SON.v(obj, _image, image)
	}

	companion object {

		const val _image = "image"
	}


}
