package com.picom.sixtysecond.model


import com.picom.sixtysecond.common.SON

import org.json.JSONObject

open class BaseItemModel : BaseModel {

	var name = ""
		// Название
		protected set

	var info = ""
		// Описание
		protected set

	constructor() {}

	constructor(obj: JSONObject?) {
		if (null == obj) return
		baseSON(obj)
	}


	override fun baseSON(obj: JSONObject) {
		super.baseSON(obj)
		name = SON.v(obj, _name, name)
		info = SON.v(obj, _info, info)
	}

	companion object {
		const val _name = "name"
		const val _info = "info"
	}
}







