package com.picom.sixtysecond.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import com.picom.sixtysecond.adapters.holders.ScoreHolder
import com.picom.sixtysecond.model.NullEntity
import com.picom.sixtysecond.model.ScoreModel
import com.picom.sixtysecond.model.TableModel
import com.picom.sixtysecond.storage.DataStore

class TableAdapter(context: Context,
				   val layoutId: Int,
				   val reloadMethod: () -> Unit)
	: IConferenceAdapter<ScoreHolder>(), Filterable {


	fun setSelectedIds(ids: String) {
		selected.change(ids)
	}

	fun setSelectedIds(ids: IntArray?) {
		selected.change(ids)
	}

	private val selected = SelectedArray()

	fun getSelectedIds(): IntArray = selected.indexes()

	private var sort = DataStore.sortNapravlenie

	fun changeSort(sortNapravlenie: Int) {
		if (sort != sortNapravlenie) {
			sort = sortNapravlenie
			notifyDataSetChanged()
		}
	}

	val changeSelectClickListener: View.OnClickListener =
			View.OnClickListener { view ->
				val x = view?.tag
				(x as? ScoreHolder)?.rebindSelectState(selected.toggle(x.id()))
			}

	private var filterList = ArrayList<ScoreModel>()

	private var table: TableModel = NullEntity.TableScore

	private val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

	private fun inflate(parent: ViewGroup): View = inflater.inflate(layoutId, parent, false)

	override fun loadIsEmpty(): Boolean = false


	override fun showLoadItem() {}

	override fun reload() {
		reloadMethod()
	}

	override fun changeArray() {}

	fun changeArray(table: TableModel) {
		this.table = table
		this.filterList = table.scores
		notifyDataSetChanged()
	}

	override fun getItem(positionA: Int): ScoreModel {
		val position =
				if (sort == DataStore.SORT_LESS)
					filterList.size - positionA - 1
				else
					positionA

		return filterList.getOrElse(position) { NullEntity.Score }
	}

	override fun hideLoadItem() {}

	override fun getItemCount(): Int = filterList.size

	override fun onBindViewHolder(holder: ScoreHolder, position: Int) {
		val item = getItem(position)
		holder.bind(item, position)
		holder.rebindSelectState(selected.state(item.getId()))
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScoreHolder {
		return ScoreHolder(inflate(parent), changeSelectClickListener)
	}

	override fun getFilter(): Filter = TableFilter(this)

	inner class TableFilter(private val adapter: TableAdapter) : Filter() {
		override fun publishResults(constraint: CharSequence, results: FilterResults) {

			if (results.count >= 0) {
				adapter.filterList = results.values as ArrayList<ScoreModel>
				adapter.notifyDataSetChanged()
			} else {
				adapter.filterList = table.scores
				adapter.notifyDataSetChanged()
			}
		}

		override fun performFiltering(constraint: CharSequence): FilterResults {
			val filterResults = FilterResults()
			filterResults.count = -1

			if (constraint.isNotEmpty()) {
				val sb = ArrayList<ScoreModel>()
				val filterPattern = constraint.toString().toLowerCase().trim()
				for (sc in table.scores) {
					if (sc.name.toLowerCase().contains(filterPattern)) {
						sb += sc
					}
				}

				filterResults.values = sb
				filterResults.count = sb.size
			}
			return filterResults
		}
	}

}
