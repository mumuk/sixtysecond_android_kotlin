package com.picom.sixtysecond.adapters

import android.support.v7.widget.RecyclerView
import com.picom.sixtysecond.storage.collection.IEntity

abstract class IConferenceAdapter<T : RecyclerView.ViewHolder> : RecyclerView.Adapter<T>() {

	/** Список содержит подзагрузку элементов   */
	var loadData = false
		set(value) {
			if (value != field) {
				field = value
				hideLoadItem()
			}
		}

	private var loading = true
		set(value) {
			if (value != field) {
				field = value
				if (field)
					hideLoadItem()
				else
					showLoadItem()
			}
		}

	var itemLoadLayoutId = 0

	fun setLoaderItemLayout(itemLoadLayoutId: Int) {
		this.itemLoadLayoutId = itemLoadLayoutId
	}

	abstract fun loadIsEmpty(): Boolean

	abstract fun reload()

	abstract fun changeArray()

	abstract fun hideLoadItem()

	abstract fun showLoadItem()

	abstract fun getItem(position: Int): IEntity

}