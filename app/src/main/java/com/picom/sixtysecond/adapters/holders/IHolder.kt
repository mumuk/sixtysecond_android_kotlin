package com.picom.sixtysecond.adapters.holders

import com.picom.sixtysecond.storage.collection.IEntity

/**
 * Created on 28.09.2018.
 * @author Kos
 */
interface IHolder<T : IEntity> {
	fun bind(entity: T, position: Int)
	fun id(): Int
}