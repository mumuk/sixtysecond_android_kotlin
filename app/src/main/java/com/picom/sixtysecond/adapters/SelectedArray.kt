package com.picom.sixtysecond.adapters

class SelectedArray {
	fun change(ids: String) {
		clear()

		try {
			ids.split(" ").map { it.toInt() }.forEach { array += it }
		} catch (e: Exception) {

		}
	}

	fun change(ids: IntArray?) {
		clear()
		ids?.forEach { array += it }
	}

	val array = HashSet<Int>()

	fun toggle(index: Int): Boolean {
		val v = !array.contains(index)
		if (v)
			array += index
		else
			array -= index
		return v
	}

	fun state(index: Int) = array.contains(index)

	fun clear() {
		array.clear()
	}

	fun indexes(): IntArray {
		val res = IntArray(array.size) { 0 }
		var i = 0
		for (a in array) {
			res[i] = a
			i += 1
		}
		return res
	}

}