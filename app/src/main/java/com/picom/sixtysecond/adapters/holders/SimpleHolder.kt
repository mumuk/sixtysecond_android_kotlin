package com.picom.sixtysecond.adapters.holders

import android.support.v7.widget.RecyclerView
import android.view.View

open class SimpleHolder(view: View) : RecyclerView.ViewHolder(view) {
}