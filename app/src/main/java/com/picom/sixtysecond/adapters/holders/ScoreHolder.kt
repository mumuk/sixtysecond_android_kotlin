package com.picom.sixtysecond.adapters.holders

import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.TextView
import com.picom.sixtysecond.R
import com.picom.sixtysecond.model.NullEntity
import com.picom.sixtysecond.model.ScoreModel
import com.picom.unionui.views.VTriangle


class ScoreHolder(view: View, listener: View.OnClickListener) : SimpleHolder(view), IHolder<ScoreModel> {

	var entity: ScoreModel = NullEntity.Score

	private val titleView = view.findViewById<TextView>(R.id.title)
	private val scoreView = view.findViewById<TextView>(R.id.score)
	private val placeView = view.findViewById<TextView>(R.id.place)
	private val tradeView = view.findViewById<VTriangle>(R.id.trade)
	private val selectView = view.findViewById<View>(R.id.topLayout)


	init {
		itemView.tag = this
		itemView.setOnClickListener(listener)
	}


	override fun bind(entity: ScoreModel, position: Int) {
		this.entity = entity


		titleView.text = entity.name
		placeView.text = entity.place
		scoreView.text = entity.scoreText()

		itemView.setBackgroundColor(

				ContextCompat.getColor(itemView.context,
						if (position % 2 == 0)
							R.color.evenRow
						else R.color.oddRow)
		)

		if (entity.getTrade() == 0) {
			tradeView.visibility = View.INVISIBLE
		} else {
			tradeView.visibility = View.VISIBLE
			tradeView.text = Math.abs(entity.getTrade()).toString()

			if (entity.getTrade() > 0) {
				tradeView.setColorAndRotate(ContextCompat.getColor(itemView.context, R.color.tradeUp), 180f)
			} else {
				tradeView.setColorAndRotate(ContextCompat.getColor(itemView.context, R.color.tradeDown), 0f)
			}
		}

	}

	override fun id(): Int = entity.getId()

	fun rebindSelectState(isSelect: Boolean) {
		if (isSelect)
			selectView.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.selectItemColor))
		else
			selectView.setBackgroundColor(Color.TRANSPARENT)
	}
}