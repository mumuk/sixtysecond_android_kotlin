package com.picom.sixtysecond.common.net

class GoodPostArguments(val infoType: String,
						val action: String,
						val ids: Set<Int>,
						val constructType: EConstructType,
						val start: (Set<Int>) -> Unit,
						val end: (Set<Int>) -> Unit
) {
	fun startLoad() {
		start(ids)
	}

	fun endLoad() {
		end(ids)
	}
}