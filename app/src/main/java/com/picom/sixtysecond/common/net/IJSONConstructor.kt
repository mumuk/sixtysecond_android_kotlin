package com.picom.sixtysecond.common.net

interface IJSONConstructor {
	fun construct(info: IContexter, obj: TJSONObject, needSave: Boolean)
}