package com.picom.sixtysecond.common

import org.json.JSONObject


class SON {
	companion object {
		fun v(obj: JSONObject, name: String, type: Int): Int {
			return obj.optInt(name, type)
		}

		fun v(obj: JSONObject, name: String, type: Long): Long {
			return obj.optLong(name, type)
		}

		fun v(obj: JSONObject, name: String, type: Float): Float {
			return obj.optDouble(name, type.toDouble()).toFloat()
		}

		fun v(obj: JSONObject, name: String, type: Double): Double {
			return obj.optDouble(name, type)
		}

		fun v(obj: JSONObject, name: String, type: Boolean): Boolean {
			return obj.optBoolean(name, type)
		}

		fun v(obj: JSONObject, name: String, type: String): String {
			return obj.optString(name, type)
		}

		fun getFloatList(obj: JSONObject, name: String): FloatArray {
			val arr = obj.optJSONArray(name)
			return if (arr != null) {
				val res = FloatArray(arr.length())
				for (i in 0 until arr.length()) {
					res[i] = arr.optDouble(i).toFloat()
				}
				res
			} else
				FloatArray(0)
		}

		fun getStringList(obj: JSONObject, name: String): Array<String> {
			val arr = obj.optJSONArray(name)
			return if (arr != null) {
				val res = Array(arr.length()) { "" }
				for (i in 0 until arr.length()) {
					res[i] = arr.optString(i)
				}
				res
			} else
				Array(0) { "" }
		}

		fun <T> getMap(obj: JSONObject, name: String, run: (JSONObject, String) -> T): ArrayList<T> {
			val arr = obj.optJSONObject(name)
			return if (arr != null) {

				val sb = ArrayList<T>()
				val it = arr.keys()
				while (it.hasNext()) {
					val i = it.next()
					val jo = arr.optJSONObject(i)

					if (jo != null) {
						sb += run(jo, i)
					}
				}
				sb
			} else
				ArrayList<T>()
		}

	}
}