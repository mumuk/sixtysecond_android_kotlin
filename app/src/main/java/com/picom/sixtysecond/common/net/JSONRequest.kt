package com.picom.sixtysecond.common.net

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request

/**
 * Класс, получающий MEDIA_TYPE_JSON
 * Created by Kos on 02.02.2015.
 */
object JSONRequest {

	private val tpe = Executors.newFixedThreadPool(2)//Отдельный поток для загрузки данных, чтобы фоновый поток не загружать

	/**
	 * Получить данные в виде json асинхронно
	 *
	 * @param info   - хранилище данных (если null, то cancelZapros)
	 * @param url    - адресс по которому загружаем (не может быть null)
	 * @param poster - сообщение и действия выполняемые с результатом (не может быть null)
	 * @return был ли запущен поток на выполнение
	 */
	fun asynh(info: IContexter?, url: String, poster: IJSONPoster): Boolean {
		if (info == null) {
			poster.cancelZapros()
			return false
		}

		return if (poster.predZapros()) {
			RequestAsyncTask(info, poster).executeOnExecutor(tpe, url)
			true
		} else {
			poster.cancelZapros()
			false
		}
	}


	/**
	 * Получить данные в виде json асинхронно
	 *
	 * @param info   - хранилище данных (если null, то cancelZapros)
	 * @param url    - адресс по которому загружаем (не может быть null)
	 * @param poster - сообщение и действия выполняемые с результатом (не может быть null)
	 * @return был ли запущен поток на выполнение
	 */
	fun asynhArray(info: IContexter?, url: String, poster: IJSONPoster): Boolean {
		if (info == null) {
			poster.cancelZapros()
			return false
		}

		return if (poster.predZapros()) {
			RequestAsyncTask(info, poster).executeOnExecutor(tpe, url)
			true
		} else {
			poster.cancelZapros()
			false
		}
	}

	/**
	 * Запрос объекта по указанному Url
	 *
	 * @param url    адрес
	 * @param poster программа обработчик события
	 * @return MEDIA_TYPE_JSON или null если ошибка
	 */
	fun dataRequest(url: String, poster: IJSONPoster, info: IContexter): TJSONObject? {

		var json: String? = ""
		var code = 0
		try {
			val client = OkHttpClient.Builder().connectTimeout(15000, TimeUnit.MILLISECONDS).build()

			val request: Request? =
					when (poster.method) {
						EMethod.post -> Request.Builder().url(url)
								.post(poster.generatePost())
								.build()
						EMethod.get -> Request.Builder()
								.url(url + poster.generateGet())
								.build()
						else -> null
					}

			if (request != null) {
				val response = client.newCall(request).execute()
				code = response.code()
				json = response.body()!!.string()
			}

		} catch (ignored: Exception) {
		}

		return try {
			if (json != null) {
				if (json.startsWith("[")) {
					TJSONObject(JSONArray(json), code)
				} else {
					TJSONObject(JSONObject(json), code)
				}
			} else
				null
		} catch (e: JSONException) {
			null
		}

	}


}
