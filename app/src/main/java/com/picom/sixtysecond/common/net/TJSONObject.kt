package com.picom.sixtysecond.common.net

import org.json.JSONArray
import org.json.JSONObject

/**
 *
 * Created by Kos on 21.04.2015.
 */
class TJSONObject {

	var isContainsAuthorized = false
		private set

	var isAuthorized = false
		private set

	var jsonObject: JSONObject? = null
		private set

	var jsonArray: JSONArray? = null
		private set

	var isSuccess = true
		private set

	var code = 0
		private set

	constructor(jsonArray: JSONArray?, code: Int = 0) {
		this.jsonArray = jsonArray
		this.code = code
	}

	constructor(jsonObject: JSONObject?, code: Int = 0) {
		this.jsonObject = jsonObject
		this.code = code

		if (jsonObject != null) {
			jsonArray = jsonObject.optJSONArray(_data)
		}
	}


	constructor(jsonObject: JSONObject?, code: Int, success: Boolean, authorized: Boolean) {
		this.isSuccess = success
		this.jsonObject = jsonObject
		this.code = code

		this.isAuthorized = authorized
		this.isContainsAuthorized = true

		if (jsonObject != null) {
			jsonArray = jsonObject.optJSONArray(_data)
		}
	}

	//	public JSONArray optJSONArray(String key) {
	//		return jsonArray;
	//	}

	fun optJSONObject(text: String): JSONObject? {
		return if (jsonObject != null) jsonObject!!.optJSONObject(text) else null
	}

	fun hasErrors(): Boolean {
		return false
	}

	fun errors(): Array<ErrorElement>? {

		return null
	}

	companion object {
		const val _data = "datas"
	}
}
