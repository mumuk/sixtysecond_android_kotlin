package com.picom.sixtysecond.common.net

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.picom.sixtysecond.model.IData
import java.net.CookieHandler

abstract class IContexter {
	abstract fun defaultParamNameValuePair(infoType: String, action: String): TPostListNameValuePair

	abstract fun sendError(error: ErrorElement)
	abstract fun apiVersion(): String
	abstract fun tableName(infoType: String): String
	abstract fun getDB(): SQLiteDatabase?
	abstract fun getWritableDB(): SQLiteDatabase?

	fun info(): IContexter = this

	fun density() = getDensity()

	private val que = ArrayList<IData>()
	private val errors = ArrayList<ErrorElement>()
	private lateinit var context: Context
	private var mDensity = ""


	fun setContext(contextElement: Context) {
		context = contextElement
		createCookies(false)
	}

	/**
	 * Функция добавляет полученные данные с сервера <br/>
	 * !!! Эта функция должна выполняться только в одном экземляре в один момент времени !!!
	 */
	fun push() {
		synchronized(this) {
			que.forEach { it.handle() }
			que.clear()

			if (errors.isNotEmpty()) {
				errors.forEach { sendError(it) }
				errors.clear()
			}
		}
	}

	fun getDensity(): String {
		return if (mDensity.isEmpty()) {
			val cont = context
			if (cont != null) {
				mDensity = cont.resources?.displayMetrics?.density?.toString() ?: "1"
				mDensity
			} else
				"1"
		} else
			mDensity
	}

	fun newElement(errorStruct: ErrorElement) {
		errors += errorStruct
	}

	fun newElement(data: IData) {
		que += data
	}

	fun getContext(): Context {
		return context
	}

	fun getCookies(): CookieHandler? {
		return null
	}

	/**
	 * Удалить все куки
	 */
	fun clearCookies() {
		createCookies(true)
	}

	fun createCookies(clear: Boolean) {

	}


	/**
	 * Элемент считается устаревшим если он старше одного дня
	 *
	 * @param value время обновления элемента
	 * @return
	 */
	fun isOld(value: Long): Boolean = value + TIME_OLD < System.currentTimeMillis()

	companion object {
		const val TIME_OLD = 24 * 60 * 60 * 1000
	}
}