package com.picom.sixtysecond.common.net

/**
 * Created by Kos on 03.12.2015.
 * Типы создания элементов
 */
enum class EConstructType {
	idType, action, arrayIdType, arrayAction
}
