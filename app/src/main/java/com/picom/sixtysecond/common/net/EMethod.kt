package com.picom.sixtysecond.common.net

/**
 * Список типов запросов
 * Created by Kos on 03.02.2015.
 */
enum class EMethod {
	post,
	get
}
