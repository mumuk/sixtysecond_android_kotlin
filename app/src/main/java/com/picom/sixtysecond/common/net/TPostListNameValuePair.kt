package com.picom.sixtysecond.common.net

import okhttp3.RequestBody

/**
 * Класс содержащий список пар наименование аргумент для формирования post-запроса
 * Created by Kos on 21.05.2015.
 */
class TPostListNameValuePair {

	private val builder: FormEncodingBuilder = FormEncodingBuilder()

	/**
	 * Добавить массив к списку элементов пар
	 * @param paramName имя параметра
	 * @param list список элементов который добавляем
	 */
	fun add(paramName: String, list: List<*>) {
		for (element in list) {
			//	add(new TNameValuePair(paramName + "[]", element.toString()));
			builder.add("$paramName[]", element.toString())
		}
	}


	/**
	 * Добавить элемент к списку элементов пар
	 * @param paramName имя параметра
	 * @param object элемент который добавляем
	 */
	fun add(paramName: String, `object`: Any) {
		builder.add(paramName, `object`.toString())
	}

	fun add(paramName: String, value: Int) {
		builder.add(paramName, value.toString())
	}

	fun add(paramName: String, value: Float) {
		builder.add(paramName, value.toString())
	}

	fun add(paramName: String, value: Double) {
		builder.add(paramName, value.toString())
	}

	fun add(paramName: String, value: Long) {
		builder.add(paramName, value.toString())
	}

	fun add(paramName: String, value: Boolean) {
		builder.add(paramName, value.toString())
	}

	fun add(paramName: String, value: String) {
		builder.add(paramName, value)
	}

	fun add(structName: String, paramName: String, value: String) {
		builder.add("$structName[$paramName]", value)
	}

	fun add(structName: String, paramName: String, elementName: String, value: String) {
		builder.add("$structName[$paramName][$elementName]", value)
	}

	fun add(structName: String, paramName: String, elementIndex: String, elementName: String, value: String) {
		builder.add(structName + "[" + paramName + "][" + elementIndex +
				"][" + elementName + "]", value)
	}

	fun add(structName: String, paramName: String, elementIndex: String, subIndex: String, elementName: String, value: String) {
		builder.add(structName + "[" + paramName + "][" + elementIndex +
				"][" + subIndex + "][" + elementName + "]", value)
	}

	fun add(names: List<String>) {
		if (names.size >= 2) {
			var s = names[0]
			for (i in 1 until names.size - 1)
				s += "[" + names[i] + "]"
			builder.add(s, names[names.size - 1])
		}
	}

	/**
	 * Добавить массив к списку элементов пар
	 * @param paramName имя параметра
	 * @param list список элементов который добавляем
	 */
	fun add(structName: String, paramName: String, list: List<*>) {
		for (element in list) {
			//	add(new TNameValuePair(paramName + "[]", element.toString()));
			builder.add("$structName[$paramName][]", element.toString())
		}
	}


	fun encodeGET(): String {
		return ""
	}

	fun addSingleItem(paramName: String, value: Int) {
		builder.add("$paramName[]", value.toString())
	}

	fun addSingleItem(structName: String, paramName: String, value: Int) {
		builder.add("$structName[$paramName][]", value.toString())
	}

	fun addSingleItem(structName: String, paramName: String, value: Long) {
		builder.add("$structName[$paramName][]", value.toString())
	}

	fun encodePost(): RequestBody {
		return builder.build()
	}

}
