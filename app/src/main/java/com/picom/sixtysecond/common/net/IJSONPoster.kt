package com.picom.sixtysecond.common.net


import okhttp3.RequestBody

/**
 * Этот класс конструирует запрос и запускает конструктор для создания объектов
 * Created by Kos on 03.02.2015.
 */
interface IJSONPoster {

	/**
	 * @return метод запроса данных
	 */
	val method: EMethod

	val jsonConstructor: IJSONConstructor
	/**
	 * Сгенирировать POST запрос.
	 * нужно проверить, что getMethod() вернёт post
	 *
	 * @return
	 */
	fun generatePost(): RequestBody

	/**
	 * Сгенерировать GET запрос.
	 * нужно проверить, что getMethod() вернёт get
	 *
	 * @return
	 */

	fun generateGet(): String

	/**
	 * Данный метод конструирует класс с ответом по полученному MEDIA_TYPE_JSON
	 *
	 * @param object
	 * @param info
	 */
	fun construct(`object`: TJSONObject?, info: IContexter)

	/**
	 * Выполняется до запроса
	 * Должен выполняться в главном потоке
	 * @return true если надо запросить, false если отмена запроса, потому что мы его уже запрашиваем
	 */
	fun predZapros(): Boolean

	/**
	 * Выполняется после запроса даже если он вернул ошибку
	 * Должен выполняться в главном потоке
	 *
	 */
	fun postZapros()

	/**
	 * Выполняется в случае если запрос был отменён.
	 * То есть когда predZapros вернул false
	 * Должен выполняться в главном потоке
	 */
	fun cancelZapros()

	/**
	 * Надо ли исполльзовать куки
	 * @return
	 */
	fun usedCookies(): Boolean

	companion object {

		const val NULL_ID = 0//Индетификатор для элемента без id
	}
}
