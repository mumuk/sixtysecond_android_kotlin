package com.picom.sixtysecond.common.net

import android.os.AsyncTask
import org.json.JSONObject

/**
 * Created by Kos on 03.02.2015.
 * Поток, загружающий данные с сервера
 */
internal class RequestAsyncTask(private val info: IContexter, private val poster: IJSONPoster) : AsyncTask<String, Void, TJSONObject>() {

	override fun doInBackground(vararg urls: String): TJSONObject? {
		val obj = loadJSON(urls[0])

		poster.construct(obj, info)

		if (obj != null) {
			if (obj.hasErrors()) {
				for (error in obj.errors()!!)
					info.newElement(error)
			}
		}
		return obj
	}

	private fun loadJSON(url: String): TJSONObject? {

		return JSONRequest.dataRequest(url, poster, info)
	}

	override fun onPostExecute(jsonData: TJSONObject?) {
		super.onPostExecute(jsonData)

		info.push()
		poster.postZapros()
	}
}