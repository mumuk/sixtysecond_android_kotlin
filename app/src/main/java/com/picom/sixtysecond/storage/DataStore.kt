package com.picom.sixtysecond.storage

import android.content.Context
import com.picom.sixtysecond.bus.BusConstants
import com.picom.sixtysecond.bus.BusProvider
import com.picom.sixtysecond.model.ChartModel
import com.picom.sixtysecond.model.NullEntity
import com.picom.sixtysecond.model.ScoreDateModel
import com.picom.sixtysecond.model.TableModel
import com.picom.sixtysecond.net.GoodApi
import com.picom.sixtysecond.net.Program
import com.picom.sixtysecond.storage.collection.ListLoadingData

object DataStore {

	val info = TAllInfo()

	fun initialize(context: Context) {
		info.setContext(context.applicationContext)
	}

	fun toggleSort() {
		sortNapravlenie = if (sortNapravlenie == SORT_GREAT) SORT_LESS else SORT_GREAT
		BusProvider.post(BusConstants.changeSort(sortNapravlenie))
	}

	const val SORT_GREAT = 0
	const val SORT_LESS = 1

	var sortNapravlenie = SORT_GREAT

	fun load(idTitle: Int, idSeason: Int) =
			scores.apply(Program.getTableId(idTitle, idSeason))


	fun loadChart(idTitle: Int, idSeason: Int) =
			charts.apply(Program.getTableId(idTitle, idSeason))


	fun getTableTitle(idTitle: Int, idSeason: Int) =
			scoreDates.get(Program.getTableId(idTitle, idSeason))

	fun reload(idTitle: Int, idSeason: Int) =
			scores.loadItem(Program.getTableId(idTitle, idSeason))

	fun getTable(idTitle: Int, idSeason: Int): TableModel =
			scores.get(Program.getTableId(idTitle, idSeason))

	fun getChart(idTitle: Int, idSeason: Int): ChartModel =
			charts.get(Program.getTableId(idTitle, idSeason))

	fun isLoadingTable(idTitle: Int, idSeason: Int): Boolean =
			scores.isLoading(Program.getTableId(idTitle, idSeason))

	fun isLoadingChart(idTitle: Int, idSeason: Int): Boolean =
			charts.isLoading(Program.getTableId(idTitle, idSeason))

	private val scores = ListLoadingData<TableModel>(
			{ NullEntity.TableScore },
			{ TableModel(it) },
			{ t, s -> GoodApi.loadTable(t, s) },
			{ BusConstants.score(it) },
			{ BusConstants.startLoad(Program._API_MAIN, it) },
			{ BusConstants.endLoad(Program._API_MAIN, it) }
	)

	private val charts = ListLoadingData<ChartModel>(
			{ NullEntity.ChartModel },
			{ ChartModel(it) },
			{ t, s -> GoodApi.loadChart(t, s) },
			{ BusConstants.score(it) },
			{ BusConstants.startLoad(Program._API_CHART, it) },
			{ BusConstants.endLoad(Program._API_CHART, it) }

	)

	private val scoreDates = ListLoadingData<ScoreDateModel>(
			{ NullEntity.ScoreDate },
			{ ScoreDateModel(it) },
			{ t, s -> GoodApi.loadTableDate(t, s) },
			{ BusConstants.scoreDate(it) },
			{ BusConstants.startLoad(Program._API_DATE, it) },
			{ BusConstants.endLoad(Program._API_DATE, it) }

	)

	fun getScoreDates() = scoreDates


}