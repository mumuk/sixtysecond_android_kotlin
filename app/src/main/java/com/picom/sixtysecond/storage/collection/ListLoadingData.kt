package com.picom.sixtysecond.storage.collection

import com.picom.sixtysecond.bus.BusProvider
import org.json.JSONObject

class ListLoadingData<T : IEntity>(val loaderItem: () -> T,
								   val constructor: (JSONObject) -> T,
								   val loadFun: (ListLoadingData<T>, Set<Int>) -> Unit,
								   val busItemUpdater: (IntArray) -> Any,
								   val busItemStartLoad: (Set<Int>) -> Any,
								   val busItemEndLoad: (Set<Int>) -> Any

) {
	private val items = HashMap<Int, T>()
	private var loadings = emptySet<Int>()

	/**
	 * Получить элемент и запросить его если нет
	 *
	 * @param id идентификатор получаемого элемента
	 * @return
	 */
	fun apply(id: Int): T = items.getOrElse(id) { loadItem(id) }


	/**
	 * Получить элемент
	 *
	 * @param id идентификатор получаемого элемента
	 * @return
	 */
	fun get(id: Int): T = items.getOrElse(id) { loaderItem() }


	fun loadItem(id: Int): T {
		startLoad(setOf(id))
		return loaderItem()
	}

	fun startLoad(ids: Set<Int>) {
		val lids = ids - loadings //получить список элементов которые ещё не загружаются
		if (lids.isNotEmpty()) {
			loadFun(this, lids)
		}
	}

	/**
	 * Эта функция вызывается когда начинается загрузка
	 *
	 * @param ids набор идентификаторов котрые загружаем
	 */
	fun loadItems(ids: Set<Int>) = startLoad(ids)


	fun put(element: T) {
		items[element.getId()] = element
	}

	fun put(elements: Array<T>, postUpdate: Boolean) {
		elements.forEach { element -> put(element) }
		if (postUpdate)
			BusProvider.post(busItemUpdater(elements.map { it.getId() }.toIntArray()))
	}


	fun removeLoad(id: Int) {
		items.remove(id)
	}

	fun clear() {
		items.clear()
	}

	fun push(items: Array<T>, postUpdate: Boolean) = ListPusher(this, items, postUpdate)


	/** Вызывается при начале загрузки
	 *
	 * @param ids идентификаторы загружаемых элементов
	 */
	fun start(ids: Set<Int>) {
		loadings += ids
		BusProvider.post(busItemStartLoad(ids))
	}

	/**Вызывается при окончании загрузки
	 *
	 * @param ids идентификаторы загружаемых элементов
	 */
	fun end(ids: Set<Int>) {
		loadings -= ids
		BusProvider.post(busItemEndLoad(ids))
	}

	fun isLoading(id: Int) = loadings.contains(id)

	fun isLoading(ids: Set<Int>) = ids.intersect(loadings).isNotEmpty()

	fun getLoadingIds() = loadings

	/**
	 * Вернуть список элементов которые ещё загружаются
	 *
	 * @param ids элементы которые проверяем на загрузку
	 * @return
	 */
	fun loading(ids: Set<Int>): Set<Int> = loadings.intersect(ids)

	/**
	 * список идентификаторов всех загруженных элементов
	 *
	 * @return
	 */
	fun allItems(): IntArray {
		return items.keys.toIntArray()
	}

	fun isEmpty() = items.isNotEmpty()
}