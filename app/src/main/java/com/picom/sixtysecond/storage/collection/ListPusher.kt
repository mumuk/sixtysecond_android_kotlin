package com.picom.sixtysecond.storage.collection

import com.picom.sixtysecond.model.IData

class ListPusher<T : IEntity>(val listData: ListLoadingData<T>, val items: Array<T>, val postUpdate: Boolean) : IData {
	override fun handle() {
		listData.put(items, postUpdate)
	}
}