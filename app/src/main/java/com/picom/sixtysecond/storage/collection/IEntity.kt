package com.picom.sixtysecond.storage.collection

interface IEntity {
	fun getId(): Int
	fun isNull(): Boolean
}