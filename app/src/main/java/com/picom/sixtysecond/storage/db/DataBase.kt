package com.picom.sixtysecond.storage.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

import com.picom.sixtysecond.net.Program
import com.picom.sixtysecond.storage.db.DataColumns.CREATE
import com.picom.sixtysecond.storage.db.DataColumns.DEFAULT_TABLE
import com.picom.sixtysecond.storage.db.DataColumns.DROP


/**
 * Created by Kos on 10.03.2016.
 */
class DataBase(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

	override fun onCreate(db: SQLiteDatabase) {
		for (table in defTables) {
			try {
				db.execSQL("$CREATE $table ($DEFAULT_TABLE )")
			} catch (ignored: Exception) {

			}

		}
	}

	override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
		for (table in defTables) {
			try {
				db.execSQL(DROP + table)
			} catch (ignored: Exception) {

			}

		}
		onCreate(db)

	}

	companion object {

		const val DATABASE_VERSION = 3
		const val DATABASE_NAME = "list2.db"
		const val TABLE_EVENT_NAME = "events_table"
		const val TABLE_PLACE_NAME = "places_table"
		const val TABLE_CHART_NAME = "chart_table"
		const val TABLE_PERSON_NAME = "persons_table"
		const val TABLE_LIST_NAME = "lists_table"
		const val TABLE_OTHER_NAME = "others_table"

		/**
		 * Список таблиц имеющих одинаковый набор столбцов projection
		 */
		val defTables = arrayOf(
				TABLE_EVENT_NAME,
				TABLE_PERSON_NAME,
				TABLE_LIST_NAME,
				TABLE_OTHER_NAME,
				TABLE_PLACE_NAME,
				TABLE_CHART_NAME
		)

		fun rowListId(infoType: String): Long {
			return when (infoType) {
				Program._API_EVENT -> 2
				else -> 1
			}
		}
	}
}