package com.picom.sixtysecond.storage

import android.database.sqlite.SQLiteDatabase
import com.picom.sixtysecond.common.net.IContexter
import com.picom.sixtysecond.common.net.ErrorElement
import com.picom.sixtysecond.common.net.TPostListNameValuePair
import com.picom.sixtysecond.net.Program
import com.picom.sixtysecond.storage.db.DataBase
import java.lang.Exception

class TAllInfo : IContexter() {
	private var api: String = ""

	override fun defaultParamNameValuePair(infoType: String, action: String): TPostListNameValuePair {
		val pair = TPostListNameValuePair()
		pair.add("arData", "infotype", infoType)
		if (action.isNotEmpty()) {
			pair.add("arData", "action", action)
		}

		return pair
	}

	override fun sendError(error: ErrorElement) {
	}

	override fun apiVersion(): String {
		if (api.isEmpty()) {
			api = try {
				val context = this.info().getContext()
				if (context != null) {
					val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
					val os = android.os.Build.VERSION.SDK_INT
					val version = pInfo.versionName
					val code = pInfo.versionCode
					"Android_${os}_${version}_${code}_"
				} else
					""
			} catch (e: Exception) {
				""
			}
		}
		return api
	}

	override fun tableName(infoType: String): String {
		return when (infoType) {
			Program._API_MAIN -> DataBase.TABLE_LIST_NAME
			Program._API_DATE -> DataBase.TABLE_PLACE_NAME
			Program._API_CHART -> DataBase.TABLE_CHART_NAME
			else -> DataBase.TABLE_OTHER_NAME
		}
	}

	override fun getDB(): SQLiteDatabase? {
		var p: SQLiteDatabase? = null

		try {
			p = DataBase(this.getContext()).readableDatabase
		} catch (e: Exception) {

		}
		return p
	}

	override fun getWritableDB(): SQLiteDatabase? {
		var p: SQLiteDatabase? = null

		try {
			p = DataBase(this.getContext()).writableDatabase
		} catch (e: Exception) {

		}
		return p
	}
}