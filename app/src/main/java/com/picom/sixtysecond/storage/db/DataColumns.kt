package com.picom.sixtysecond.storage.db

import android.provider.BaseColumns

object DataColumns : BaseColumns {

	const val COLUMN_VALUE = "value"
	const val COLUMN_MODTIME = "modtime"
	val onlyIds = arrayOf("_id")
	val projection = arrayOf("_id", "modtime", "value")
	const val PRIMARY_KEY = " INTEGER PRIMARY KEY"
	const val TEXT_TYPE = " TEXT"
	const val BOOL_TYPE = " INTEGER"
	const val INTEGER_TYPE = " INTEGER"
	const val COMMA_SEP = ","
	const val DROP = "DROP TABLE IF EXISTS "
	const val CREATE = "CREATE TABLE "
	const val DEFAULT_TABLE = "_id INTEGER PRIMARY KEY,modtime INTEGER,value TEXT"

}